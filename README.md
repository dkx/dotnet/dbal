# DKX/Dbal

Strongly opinionated database abstraction layer with simple object mapping (not an ORM).

* Async only
* No method extensions
* Mapping to immutable classes via constructors
* ~~No-setters mapping~~ see [property mapping](./docs/property-mapping.md)
* With middlewares
* Opening connection automatically when needed
* Automatic nested transactions (with savepoints)
* Native support for PostgreSQL enums
* Native SQL query builder

## Installation

```bash
dotnet add package DKX.Dbal
```

## Basic usage

```c#
using DKX.Dbal;

class User
{
    public User(long id, string email)
    {
        Id = id;
        Email = email;
    }

    public long Id { get; }

    public string Email { get; }
}

var driver = new AdoDriver(new SqliteConnection("Data Source=:memory:"));
var configuration = new Configuration
{
    CommandTimeout = 30,
    TransactionIsolationLevel = IsolationLevel.Snapshot,
}

await using var connection = new Connection(driver, configuration);

await foreach (var user in connection.Query<User>("SELECT id, email FROM user"))
{
    Console.WriteLine(user.Email);
}
```

## Features

* [Supported drivers](./docs/drivers.md)
* [Queries](./docs/queries.md)
* [Transactions](./docs/transactions.md)
* [Query builder](./docs/query-builder.md)
* [Middlewares](./docs/middlewares.md)
* [Property mapping](./docs/property-mapping.md)
