# Queries

## Execute

```c#
var affectedRows = await connection.Execute("UPDATE users SET email_verified = TRUE WHERE email = 'john@doe.com'");
```

## Execute scalar

```c#
var totalCount = await connection.ExecuteScalar<int>("SELECT COUNT(*) FROM users");
```

The `ExecuteScalar` method is only a shortcut and that applies to all the following methods. Full code would look like 
this:

```c#
var totalCount = await connection
    .CreateQuery("SELECT COUNT(*) FROM users")
    .ToScalar<int>();
```

The `CreateQuery` method can be slightly faster and gives you more options. **It is the preferred way of querying data.**

## Query scalar

`QueryScalar` is similar to `ExecuteScalar`, but returns `IAsyncEnumerable<TResult>`.

```c#
await foreach (var email in connection.QueryScalar<string>("SELECT email FROM users"))
{
    
}
```

## Query

All `Query*` methods will map the rows into given class type constructor.

**There is no mapping by name, values are mapped into constructor by their position in given query! You should have 
tests, especially when using this library.**

```c#
await foreach (var user in connection.Query<User>("SELECT id, email FROM users"))
{
    
}
```

## Query single or default

* Throws an exception if more than 1 row is returned
* Returns `null` if no row is returned

```c#
User? user = await connection.QuerySingleOrDefault("SELECT id, email FROM users WHERE email = 'john@doe.com'");
```

## Query single

* Throws an exception if more than 1 row is returned
* Throws an exception if no row is returned

```c#
User user = await connection.QuerySingle("SELECT id, email FROM users WHERE email = 'john@doe.com'");
```

## Query chunks

Similar to `Query<TResult>` method, but suitable for large data sets. It works by fetching one chunk of data at a time
(using `LIMIT` and `OFFSET` clauses), processing it and then fetching another chunk until there is zero rows returned 
from database.

```c#
var chunkSize = 100;

await foreach (var user in connection.QueryChunks<User>("SELECT id, email FROM users", chunkSize))
{
    
}
```

## Parameters and prepared statements

* Prepared statements is used automatically when some parameter is passed into query

```c#
await connection.Execute("INSERT INTO users (email) VALUES (@Email)", new
{
    Email = "john@doe.com",
});
```

## Stored procedures

```c#
await connection.Execute("Users_List", commandType: CommandType.StoredProcedure);
```

## Naming queries

You can pass `name` parameter into any method mentioned above and use it later with logging (see
[middlewares](./middlewares.md)).

Name is not used anywhere by this library.

## Anonymous mapping

All `Query*` methods can be used without providing specific type (generic `TResult`), in which case the
`IImmutableDictionary<string, object?>` is returned.
