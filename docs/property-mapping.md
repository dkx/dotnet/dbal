# Property mapping

This whole library was build to be immutable only. That is why there was no property mapping at first, but we have new 
options with C# 9 (records and init only properties).

## Installation

**Only for .NET >= 5.0**

```bash
$ dotnet add package DKX.Dbal.Mapping
```

## Usage

**Define record:**

```c#
using DKX.Dbal.Mapping.Attributes;

[Table("user")]
public record User
{
    [Id("id")]
    public Guid Id { get; init; }
    
    [Column("email")]
    public string Email { get; init; }
}
```

**Select:**

```c#
await connection
    .CreateQueryBuilder()
    .Select<User>()
    .ToQuery()
    .Execute();

// SQL: SELECT id, email FROM user
```

or with an alias:

```c#
await connection
    .CreateQueryBuilder()
    .Select<User>("u")
    .ToQuery()
    .Execute();

// SQL: SELECT u.id, u.email FROM user u
```

**Insert:**

```c#
await connection
    .CreateQueryBuilder()
    .Insert(new User {Id = Guid.NewGuid(), Email = "john@doe.com"})
    .ToQuery()
    .Execute();

// SQL: INSERT INTO user (id, email) VALUES (@id, @email)
```

or with shortcut:

```c#
await connection.Insert(new User {Id = Guid.NewGuid(), Email = "john@doe.com"}));
```

**Update:**

```c#
var user = new User {Id = Guid.NewGuid(), Email = "john@doe.com"};

await connection
    .CreateQueryBuilder()
    .Update(user)
    .Where("id = @Id").SetParameter("@Id", user.Id)
    .ToQuery()
    .Execute();

// SQL: UPDATE user SET email = @email WHERE id = @Id
```

**Delete:**

```c#
var user = new User {Id = Guid.NewGuid(), Email = "john@doe.com"};

await connection
    .CreateQueryBuilder()
    .Delete<User>()
    .Where("id = @Id").SetParameter("@Id", user.Id)
    .ToQuery()
    .Execute();

// SQL: DELETE FROM user WHERE id = @Id
```

## Embeddable

It's possible to separate parts of entity into embeddable which can be used in other entities.

```c#
[Table("user")]
public record User
{
    [Id("id")]
    public Guid Id { get; init; }
    
    [Embedded(typeof(AddressMapping))]
    public Address Address { get; init; }
}
```

**Configure Address mapping:**

```c#
private class AddressMapping : IEmbeddable<Address>
{
    // Define names of columns you want to consume (without any prefix)
    public IEnumerable<string> Columns { get; } = new []
    {
        "town",
        "street",
    };

    // Create full database column name from property name (group) and column name defined above
    // group is by default name of the property (in this case "Address")
    // it can be changed by providing 2nd argument to [Embedded] attribute - eg. [Embedded(typeof(AddressMapping), "Addr")]
    public string CreateFullColumnName(string group, string column)
    {
        return $"{target}_{column}";
    }

    // Create Address type if town and street is not null
    public Address? ToEmbedded(IDictionary<string, object?> values)
    {
        if (values["town"] == null || values["street"] == null)
        {
            return null;
        }
        
        return new()
        {
            Town = (string) values["town"]!,
            Street = (string) values["street"]!,
        };
    }

    // Transform Address type to database values
    public IDictionary<string, object?> ToDatabaseValue(Address data)
    {
        return new Dictionary<string, object?>
        {
            ["town"] = data.Town,
            ["street"] = data.Street,
        };
    }
}
```

## Transformers

Transformers are similar to embeddables but unlike them, they map one column to one application type.

```c#
[Table("user")]
public record User
{
    [Id("id")]
    public Guid Id { get; init; }
    
    [Column("email"), typeof(EmailTransformer)]
    public Email Email { get; init; }
}

public readonly struct Email
{
    public Email(string value)
    {
        Value = value;
    }
    
    public string Value { get; }
}

public class EmailTransformer : ITransformer<Email>
{
    public Email ToNetValue(object value)
    {
        if (value is string str)
        {
            return new Email(str);
        }
    
        throw new NotSupportedException();
    }
    
    public object ToDatabaseValue(Email value)
    {
        return value.Value;
    }
}
```

## Hidden columns

Whenever there is some data which can not be mapped to any property an exception is thrown. If you still want to 
process such query, you can hide these fields from property mapping.

```c#
await connection.CreateQueryBuilder()
    .Select("id", "email", "NOW()")
    .From<User>()
    .WithHiddenColumns("NOW()");
```

or with alias:

```c#
await connection.CreateQueryBuilder()
    .Select("id", "email", "NOW() AS hiddenCurrentTime")
    .From<User>()
    .WithHiddenColumns("hiddenCurrentTime");
```
