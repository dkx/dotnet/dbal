# Supported drivers

Any ADO.NET database provider will work.

```c#
using DKX.Dbal;

var driver = new AdoDriver(myAdoNetDatabaseProvider);
```

## Postgres & Npgsql

[Npgsql](https://www.npgsql.org/index.html) is also an ADO.NET provider so you can use the default `AdoDriver` mentioned
above.

But there is also specialized driver just for Npgsql with support for more features.

**Install driver:**

```bash
dotnet add package DKX.Dbal.Npgsql
```

**And use:**

```c#
using DKX.Dbal.Npgsql;
using Npgsql;

var driver = new NpgsqlDriver(new NpgsqlConnection(myConnectionString));
```
