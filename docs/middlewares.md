# Middlewares

```c#
using DKX.Dbal;

class LoggingMiddleware : IMiddleware
{
    public async Task<TResult> Handle<TResult>(IContext context, Next<TResult> next, CancellationToken cancellationToken)
    {
        Console.WriteLine($"Query: [{context.Name}] {context.Query}");

        // Call next middleware or execute database query if this is the last middleware
        var result = await next();

        Console.WriteLine($"Finished query: {context.Query}");

        return result;
    }
}

connection.AddMiddleware(new LoggingMiddleware());
```

`context.Name` in the example above is a name of query - see "Naming queries" in [queries](./queries.md).
