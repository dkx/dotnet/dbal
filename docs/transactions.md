# Transactions

```c#
await connection.Transactional(async () => {
    await connection.Execute("INSERT INTO users (id, email) VALUES (@Id, @Email)", new {Id = id, Email = email});
});
```

## Full example

```c#
await using (var transaction = await connection.BeginTransaction())
{
    try {
        await connection.Execute("INSERT INTO users (id, email) VALUES (@Id, @Email)", new {Id = id, Email = email});
        await transaction.Commit();
    } catch (Exception)
    {
        transaction.Rollback();
        throw;
    }
}
```

## Nested transactions

This library supports nested transactions using savepoints. This is supported only in the [NpgsqlDriver](./drivers.md)
for now.

```c#
await using (var transaction1 = await connection.BeginTransaction())
{
    // todo

    await using (var transaction2 = await connection.BeginTransaction())
    {
        // todo

        await using (var transaction3 = await connection.BeginTransaction())
        {
            // todo

            await using (var transaction4 = await connection.BeginTransaction())
            {
                // todo
            
                await transaction4.Commit();
            }
        
            await transaction3.Rollback();
        }
    
        await transaction2.Commit();
    }

    await transaction1.Commit();
}
```
