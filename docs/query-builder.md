# Query builder

```c#
var query = connection.CreateQueryBuilder()
    .Select("u.id")
    .From("users", "u")
    .Where("u.verified = @Verified")
    .AndWhere("u.deleted = @Deleted")
    .SetParameter("Verified", true)
    .SetParameter("Deleted", false)
    .ToQuery();

await foreach (var user in query.ToResult<User>(sql))
{
    
}
```

## Expression builder

Expression builder can be used for more complex `WHERE` clauses (or `HAVING`). They can be nested indefinitely.

The example above can be written with expression builder like this:

```c#
var qb = connection.CreateQueryBuilder();
var query = qb
    .Select("u.id")
    .From("users", "u")
    .Where(qb.Expr.And(
        "u.verified = @Verified",
        "u.deleted = @Deleted"
    ))
    .SetParameter("Verified", true)
    .SetParameter("Deleted", false)
    .ToQuery();

// todo
```

See tests for more examples.
