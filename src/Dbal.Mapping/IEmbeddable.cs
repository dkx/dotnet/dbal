using System.Collections.Generic;

namespace DKX.Dbal.Mapping
{
	public interface IEmbeddable<TTarget>
		where TTarget : class
	{
		IEnumerable<string> Columns { get; }

		string CreateFullColumnName(string group, string column);

		TTarget? ToEmbedded(IDictionary<string, object?> values);

		IDictionary<string, object?> ToDatabaseValue(TTarget data);
	}
}
