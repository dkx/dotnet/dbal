using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DKX.Dbal.Mapping.Properties;

namespace DKX.Dbal.Mapping.Data
{
	public sealed class Row<TRow>
		where TRow : class
	{
		private readonly IList<AbstractProperty> _properties;
		
		private readonly IDictionary<string, string> _aliases;

		private readonly IDictionary<string, object?> _columns = new Dictionary<string, object?>();

		public Row(IDataRecord record, IDictionary<string, string> aliases, string[] hiddenColumns)
		{
			_properties = PropertiesLoader.Load<TRow>().ToList();
			_aliases = aliases;
			
			for (var i = 0; i < record.FieldCount; i++)
			{
				var name = record.GetName(i);
				if (hiddenColumns.Contains(name))
				{
					continue;
				}
				
				var value = record.IsDBNull(i) ? null : record.GetValue(i);

				_columns.Add(name, value);
			}
		}

		internal void Map(TRow into)
		{
			foreach (var property in _properties)
			{
				property.Consume(this, into);
			}

			if (_columns.Count > 0)
			{
				throw new Exception($"Can not map record to '{typeof(TRow).FullName}', there are no writable properties for columns: {string.Join(", ", _columns.Select(c => c.Key))}");
			}
		}

		public bool HasColumn(string column)
		{
			if (_aliases.ContainsKey(column))
			{
				column = _aliases[column];
			}
			
			return _columns.ContainsKey(column);
		}

		public object? Consume(string column)
		{
			if (!HasColumn(column))
			{
				throw new Exception($"Can not map record to '{typeof(TRow).FullName}', there is no column '{column}'");
			}
			
			if (_aliases.ContainsKey(column))
			{
				column = _aliases[column];
			}

			var value = _columns[column]!;
			_columns.Remove(column);

			return value;
		}
	}
}
