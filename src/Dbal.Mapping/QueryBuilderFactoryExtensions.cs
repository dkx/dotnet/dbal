using DKX.Dbal.Mapping.Attributes;
using DKX.Dbal.Mapping.Properties;
using DKX.Dbal.Queries;

namespace DKX.Dbal.Mapping
{
	public static class QueryBuilderFactoryExtensions
	{
		public static ISelectQueryBuilder Select<TRow>(this IQueryBuilderFactory factory, string? alias = null)
			where TRow : class
		{
			return factory
				.Select()
				.From<TRow>(alias)
				.AddColumns<TRow>(alias);
		}

		public static IInsertQueryBuilder Insert<TRow>(this IQueryBuilderFactory factory, TRow entity)
			where TRow : class
		{
			var table = TableAttribute.Load<TRow>();
			var properties = PropertiesLoader.Load<TRow>();
			var qb = factory.Insert(table.Name);

			var i = 0;
			foreach (var property in properties)
			{
				foreach (var column in property.GetColumns())
				{
					var realColumn = property.GetRealColumnName(column);
					var key = $"@p{i++}";

					qb = qb
						.SetValue(realColumn, key)
						.SetParameter(key, property.GetColumnValue(entity, column));
				}
			}

			return qb;
		}

		public static IUpdateQueryBuilder Update<TRow>(this IQueryBuilderFactory factory, string? alias = null)
			where TRow : class
		{
			var table = TableAttribute.Load<TRow>();
			return factory.Update(table.Name, alias);
		}

		public static IUpdateQueryBuilder Update<TRow>(this IQueryBuilderFactory factory, TRow entity, string? alias = null)
			where TRow : class
		{
			var qb = factory.Update<TRow>(alias);
			var properties = PropertiesLoader.Load<TRow>();

			var i = 0;
			foreach (var property in properties)
			{
				if (!property.ShouldBeInUpdate())
				{
					continue;
				}
				
				foreach (var column in property.GetColumns())
				{
					var realColumn = property.GetRealColumnName(column);
					var key = $"@p{i++}";

					qb = qb
						.Set(realColumn, key)
						.SetParameter(key, property.GetColumnValue(entity, column));
				}
			}

			return qb;
		}

		public static IDeleteQueryBuilder Delete<TRow>(this IQueryBuilderFactory factory)
			where TRow : class
		{
			var table = TableAttribute.Load<TRow>();
			return factory.Delete(table.Name);
		}
	}
}
