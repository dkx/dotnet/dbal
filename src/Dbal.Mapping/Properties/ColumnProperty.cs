using System;
using System.Collections.Generic;
using System.Reflection;
using DKX.Dbal.Mapping.Data;

namespace DKX.Dbal.Mapping.Properties
{
	internal class ColumnProperty<TType> : AbstractProperty
		where TType : notnull
	{
		private readonly string _name;

		private readonly Type? _transformerType;

		private ITransformer<TType>? _transformer;

		public ColumnProperty(PropertyInfo property, string name, Type? transformerType = null) : base(property)
		{
			_name = name;
			_transformerType = transformerType;
		}

		public override IEnumerable<string> GetColumns()
		{
			return new[] {_name};
		}

		public override object? GetColumnValue<TRow>(TRow row, string name)
		{
			var value = Property.GetValue(row);
			if (value == null)
			{
				return null;
			}
			
			var transformer = GetTransformer();

			return transformer != null
				? transformer.ToDatabaseValue((TType) value)
				: value;
		}

		public override void Consume<TRow>(Row<TRow> data, TRow row)
		{
			if (!data.HasColumn(_name))
			{
				return;
			}

			var value = data.Consume(_name);
			if (value == null)
			{
				Property.SetValue(row, null);
				return;
			}
			
			var transformer = GetTransformer();
			Property.SetValue(row, transformer == null ? value : transformer.ToNetValue(value));
		}

		private ITransformer<TType>? GetTransformer()
		{
			if (_transformer != null)
			{
				return _transformer;
			}

			if (_transformerType == null)
			{
				return null;
			}

			return _transformer = Activator.CreateInstance(_transformerType) as ITransformer<TType>;
		}
	}
}
