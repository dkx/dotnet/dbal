using System;
using System.Collections.Generic;
using System.Reflection;
using DKX.Dbal.Mapping.Data;

namespace DKX.Dbal.Mapping.Properties
{
	internal sealed class EmbeddedProperty<TTarget> : AbstractProperty
		where TTarget : class
	{
		private readonly Type _mapperType;

		private readonly string _group;

		private IEmbeddable<TTarget>? _mapper;
		
		public EmbeddedProperty(PropertyInfo property, Type mapperType, string? group = null) : base(property)
		{
			_mapperType = mapperType;
			_group = group ?? Property.Name;
		}

		public override IEnumerable<string> GetColumns()
		{
			return GetMapper().Columns;
		}

		public override string GetRealColumnName(string name)
		{
			return GetMapper().CreateFullColumnName(_group, name);
		}

		public override object? GetColumnValue<TRow>(TRow row, string name)
		{
			var mapper = GetMapper();
			var data = Property.GetValue(row) as TTarget;
			if (data == null)
			{
				return null;
			}
			
			return mapper.ToDatabaseValue(data)[name];
		}

		public override void Consume<TRow>(Row<TRow> data, TRow row)
		{
			var mapper = GetMapper();
			var columns = mapper.Columns;
			var values = new Dictionary<string, object?>();

			foreach (var name in columns)
			{
				var real = mapper.CreateFullColumnName(_group, name);
				
				if (!data.HasColumn(real))
				{
					return;
				}

				values.Add(name, data.Consume(real));
			}

			var target = mapper.ToEmbedded(values);

			Property.SetValue(row, target);
		}

		private IEmbeddable<TTarget> GetMapper()
		{
			return _mapper ??= (Activator.CreateInstance(_mapperType) as IEmbeddable<TTarget>)!;
		}
	}
}
