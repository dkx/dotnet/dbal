using System.Collections.Generic;
using System.Reflection;
using DKX.Dbal.Mapping.Data;

namespace DKX.Dbal.Mapping.Properties
{
	internal abstract class AbstractProperty
	{
		protected readonly PropertyInfo Property;

		protected AbstractProperty(PropertyInfo property)
		{
			Property = property;
		}

		public abstract IEnumerable<string> GetColumns();

		public abstract object? GetColumnValue<TRow>(TRow row, string name)
			where TRow : class;

		public abstract void Consume<TRow>(Row<TRow> data, TRow row)
			where TRow : class;

		public virtual string GetRealColumnName(string name)
		{
			return name;
		}

		public virtual bool ShouldBeInUpdate()
		{
			return true;
		}
	}
}
