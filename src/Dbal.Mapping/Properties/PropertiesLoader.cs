using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DKX.Dbal.Mapping.Attributes;

namespace DKX.Dbal.Mapping.Properties
{
	internal static class PropertiesLoader
	{
		public static IEnumerable<AbstractProperty> Load<TClass>()
			where TClass : class
		{
			var idType = typeof(IdAttribute);
			var columnType = typeof(ColumnAttribute);
			var embeddedType = typeof(EmbeddedAttribute);

			var properties = typeof(TClass)
				.GetProperties(BindingFlags.Public | BindingFlags.Instance)
				.Where(p => p.CanRead);

			var result = new List<AbstractProperty>();

			foreach (var property in properties)
			{
				var nullableType = Nullable.GetUnderlyingType(property.PropertyType);
				var propertyType = nullableType ?? property.PropertyType;
				
				if (Attribute.IsDefined(property, idType))
				{
					var attribute = (Attribute.GetCustomAttribute(property, idType) as IdAttribute)!;
					var id = Activator.CreateInstance(
						typeof(IdProperty<>).MakeGenericType(propertyType),
						property,
						attribute.Name
					) as AbstractProperty;

					result.Add(id!);
					continue;
				}
				
				if (Attribute.IsDefined(property, columnType))
				{
					var attribute = (Attribute.GetCustomAttribute(property, columnType) as ColumnAttribute)!;
					var column = Activator.CreateInstance(
						typeof(ColumnProperty<>).MakeGenericType(propertyType),
						property,
						attribute.Name,
						attribute.TransformerType
					) as AbstractProperty;

					result.Add(column!);
					continue;
				}

				if (Attribute.IsDefined(property, embeddedType))
				{
					var attribute = (Attribute.GetCustomAttribute(property, embeddedType) as EmbeddedAttribute)!;
					var embedded = Activator.CreateInstance(
						typeof(EmbeddedProperty<>).MakeGenericType(propertyType),
						property,
						attribute.Mapper,
						attribute.Group
					) as AbstractProperty;
					
					result.Add(embedded!);
				}
			}

			return result;
		}
	}
}
