using System.Reflection;

namespace DKX.Dbal.Mapping.Properties
{
	internal sealed class IdProperty<TType> : ColumnProperty<TType>
	{
		public IdProperty(PropertyInfo property, string name) : base(property, name)
		{
		}

		public override bool ShouldBeInUpdate()
		{
			return false;
		}
	}
}
