using System;

namespace DKX.Dbal.Mapping.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class TableAttribute : Attribute
	{
		public TableAttribute(string name)
		{
			Name = name;
		}
		
		public string Name { get; }

		internal static TableAttribute Load<TClass>()
			where TClass : class
		{
			var type = typeof(TClass);
			var tableType = typeof(TableAttribute);
			
			if (!IsDefined(type, tableType))
			{
				throw new Exception($"DKX.Dbal.Mapping: can not map data to class '{type.FullName}', it is missing a [Table] attribute");
			}

			return (GetCustomAttribute(type, tableType) as TableAttribute)!;
		}
	}
}
