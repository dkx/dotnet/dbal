using System;

namespace DKX.Dbal.Mapping.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class ColumnAttribute : Attribute
	{
		public ColumnAttribute(string name, Type? transformerType = null)
		{
			Name = name;
			TransformerType = transformerType;
		}
		
		public string Name { get; }
		
		public Type? TransformerType { get; }
	}
}
