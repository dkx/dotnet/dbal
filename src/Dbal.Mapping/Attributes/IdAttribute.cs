using System;

namespace DKX.Dbal.Mapping.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class IdAttribute : Attribute
	{
		public IdAttribute(string name)
		{
			Name = name;
		}
		
		public string Name { get; }
	}
}
