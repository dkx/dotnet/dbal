using System;

namespace DKX.Dbal.Mapping.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class EmbeddedAttribute : Attribute
	{
		public EmbeddedAttribute(Type mapper, string? group = null)
		{
			Mapper = mapper;
			Group = group;
		}
		
		public Type Mapper { get; }
		
		public string? Group { get; }
	}
}
