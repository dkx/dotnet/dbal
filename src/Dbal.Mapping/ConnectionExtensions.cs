using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal.Mapping
{
	public static class ConnectionExtensions
	{
		public static Task<int> Insert<TRow>(this IConnection connection, TRow entity, CancellationToken cancellationToken = default)
			where TRow : class
		{
			return connection
				.CreateQueryBuilder()
				.Insert(entity)
				.ToQuery()
				.Execute(cancellationToken);
		}
	}
}
