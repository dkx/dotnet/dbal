using System;
using System.Collections.Generic;
using DKX.Dbal.Mapping.Attributes;
using DKX.Dbal.Mapping.Properties;
using DKX.Dbal.Queries;

namespace DKX.Dbal.Mapping
{
	public static class SelectQueryBuilderExtensions
	{
		public static ISelectQueryBuilder From<TRow>(this ISelectQueryBuilder qb, string? alias = null)
			where TRow : class
		{
			return qb
				.From(TableAttribute.Load<TRow>().Name, alias)
				.WithRowMapper(new PropertyRowMapper());
		}

		public static ISelectQueryBuilder WithHiddenColumns(this ISelectQueryBuilder qb, params string[] columns)
		{
			if (!(qb.RowMapper is PropertyRowMapper rowMapper))
			{
				throw new NotSupportedException("Hidden columns are supported only with property mapped results");
			}

			return qb.WithRowMapper(
				rowMapper.WithHiddenColumns(columns)
			);
		}

		public static ISelectQueryBuilder AddColumns<TRow>(this ISelectQueryBuilder qb, string? alias = null)
			where TRow : class
		{
			var properties = PropertiesLoader.Load<TRow>();
			var aliases = new Dictionary<string, string>();
			var i = 0;
			
			foreach (var property in properties)
			{
				foreach (var column in property.GetColumns())
				{
					var realColumn = property.GetRealColumnName(column);
					var key = $"c{i++}";

					aliases.Add(realColumn, key);

					qb = alias == null
						? qb.AddColumns($"{realColumn} {key}")
						: qb.AddColumns($"{alias}.{realColumn} {key}");
				}
			}

			return qb.WithRowMapper(new PropertyRowMapper(aliases));
		}

		public static ISelectQueryBuilder Join<TTable>(this ISelectQueryBuilder qb, string fromAlias, string alias, string? condition = null)
			where TTable : class
		{
			return qb.Join(fromAlias, TableAttribute.Load<TTable>().Name, alias, condition);
		}

		public static ISelectQueryBuilder InnerJoin<TTable>(this ISelectQueryBuilder qb, string fromAlias, string alias, string? condition = null)
			where TTable : class
		{
			return qb.InnerJoin(fromAlias, TableAttribute.Load<TTable>().Name, alias, condition);
		}

		public static ISelectQueryBuilder LeftJoin<TTable>(this ISelectQueryBuilder qb, string fromAlias, string alias, string? condition = null)
			where TTable : class
		{
			return qb.LeftJoin(fromAlias, TableAttribute.Load<TTable>().Name, alias, condition);
		}

		public static ISelectQueryBuilder RightJoin<TTable>(this ISelectQueryBuilder qb, string fromAlias, string alias, string? condition = null)
			where TTable : class
		{
			return qb.RightJoin(fromAlias, TableAttribute.Load<TTable>().Name, alias, condition);
		}
	}
}
