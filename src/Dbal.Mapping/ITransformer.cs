namespace DKX.Dbal.Mapping
{
	public interface ITransformer<TType>
	{
		TType ToNetValue(object value);

		object ToDatabaseValue(TType value);
	}
}
