using System;
using System.Collections.Generic;
using System.Data;
using DKX.Dbal.Mapping.Data;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Mapping
{
	internal sealed class PropertyRowMapper : IRowMapper
	{
		private readonly IDictionary<string, string> _aliases;

		private readonly string[] _hiddenColumns;
		
		public PropertyRowMapper(
			IDictionary<string, string>? aliases = null,
			string[]? hiddenColumns = null
		)
		{
			_aliases = aliases ?? new Dictionary<string, string>();
			_hiddenColumns = hiddenColumns ?? Array.Empty<string>();
		}

		public PropertyRowMapper WithHiddenColumns(params string[] columns)
		{
			return new PropertyRowMapper(_aliases, columns);
		}

		public TResult Map<TResult>(Type resultType, IDataRecord record)
			where TResult : class
		{
			var result = (TResult) Activator.CreateInstance(resultType)!;
			var row = new Row<TResult>(record, _aliases, _hiddenColumns);

			row.Map(result);

			return result;
		}
	}
}
