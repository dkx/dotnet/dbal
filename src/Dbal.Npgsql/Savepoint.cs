using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Npgsql;

namespace DKX.Dbal.Npgsql
{
	public class Savepoint : ISavepoint
	{
		private readonly Transaction _transaction;

		private readonly NpgsqlTransaction _dbTransaction;

		private readonly string _name;

		private readonly Action _closed;

		private bool _isClosed;

		public Savepoint(Transaction transaction, NpgsqlTransaction dbTransaction, string name, Action closed)
		{
			_transaction = transaction;
			_dbTransaction = dbTransaction;
			_name = name;
			_closed = closed;
		}
		
		public ValueTask DisposeAsync()
		{
			Close();
			return default;
		}

		public uint SavepointsCount => _transaction.SavepointsCount;

		public async Task Commit(CancellationToken cancellationToken = default)
		{
			await _dbTransaction.ReleaseAsync(_name, cancellationToken);
			Close();
		}

		public async Task Rollback(CancellationToken cancellationToken = default)
		{
			await _dbTransaction.RollbackAsync(_name, cancellationToken);
			Close();
		}

		public bool SupportsSavepoints()
		{
			return _transaction.SupportsSavepoints();
		}

		public Task<ISavepoint> CreateSavepoint(string name, CancellationToken cancellationToken = default)
		{
			return _transaction.CreateSavepoint(name, cancellationToken);
		}

		public DbTransaction ToDbTransaction()
		{
			return _transaction.ToDbTransaction();
		}

		private void Close()
		{
			if (_isClosed)
			{
				return;
			}

			_closed();
			_isClosed = true;
		}
	}
}
