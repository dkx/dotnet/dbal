using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Npgsql;

namespace DKX.Dbal.Npgsql
{
	public class Transaction : ITransaction
	{
		private readonly NpgsqlTransaction _transaction;

		private readonly Action _closed;

		private bool _isClosed;
		
		public Transaction(NpgsqlTransaction transaction, Action closed)
		{
			_transaction = transaction;
			_closed = closed;
		}
		
		public async ValueTask DisposeAsync()
		{
			await _transaction.DisposeAsync();
			Close();
		}

		public uint SavepointsCount { get; private set; }

		public async Task Commit(CancellationToken cancellationToken = default)
		{
			await _transaction.CommitAsync(cancellationToken);
			Close();
		}

		public async Task Rollback(CancellationToken cancellationToken = default)
		{
			await _transaction.RollbackAsync(cancellationToken);
			Close();
		}

		public bool SupportsSavepoints()
		{
			return true;
		}

		public async Task<ISavepoint> CreateSavepoint(string name, CancellationToken cancellationToken = default)
		{
			await _transaction.SaveAsync(name, cancellationToken);
			SavepointsCount++;
			return new Savepoint(this, _transaction, name, () => SavepointsCount--);
		}

		public DbTransaction ToDbTransaction()
		{
			return _transaction;
		}

		private void Close()
		{
			if (_isClosed)
			{
				return;
			}

			_closed();
			_isClosed = true;
		}
	}
}
