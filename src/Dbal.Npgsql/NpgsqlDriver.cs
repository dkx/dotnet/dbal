using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal.Drivers;
using Npgsql;

namespace DKX.Dbal.Npgsql
{
	public class NpgsqlDriver : AdoDriver
	{
		private readonly NpgsqlConnection _connection;

		public NpgsqlDriver(NpgsqlConnection connection) : base(connection)
		{
			_connection = connection;
		}

		public override async Task<ITransaction> BeginTransaction(IsolationLevel isolationLevel, Action closed, CancellationToken cancellationToken = default)
		{
			return new Transaction(await _connection.BeginTransactionAsync(isolationLevel, cancellationToken), closed);
		}
	}
}
