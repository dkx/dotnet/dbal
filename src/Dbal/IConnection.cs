using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;

namespace DKX.Dbal
{
	public interface IConnection : IDisposable, IAsyncDisposable
	{
		IDriver ToDriver();
		
		ValueTask Connect(CancellationToken cancellationToken = default);

		ValueTask Disconnect();

		Task Reconnect(CancellationToken cancellationToken = default);

		Task<ITransaction> BeginTransaction(CancellationToken cancellationToken = default);

		Task Transactional(Func<Task> fn, CancellationToken cancellationToken = default);

		Task<TResult> Transactional<TResult>(Func<Task<TResult>> fn, CancellationToken cancellationToken = default);

		void AddMiddleware(IMiddleware middleware);

		IQueryBuilderFactory CreateQueryBuilder();

		IQuery CreateQuery(string sql, CommandType commandType = CommandType.Text);
		
		Task<int> Execute(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);

		Task<TResult> ExecuteScalar<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);

		IAsyncEnumerable<TResult> QueryScalar<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);

		IAsyncEnumerable<TResult> Query<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class;

		IAsyncEnumerable<IImmutableDictionary<string, object?>> Query(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);
		
		Task<TResult?> QuerySingleOrDefault<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class;

		Task<IImmutableDictionary<string, object?>?> QuerySingleOrDefault(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);

		Task<TResult> QuerySingle<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class;

		Task<IImmutableDictionary<string, object?>> QuerySingle(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);

		public IAsyncEnumerable<TResult> QueryChunks<TResult>(string sql, int chunkSize, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class;

		public IAsyncEnumerable<IImmutableDictionary<string, object?>> QueryChunks(string sql, int chunkSize, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default);
	}
}
