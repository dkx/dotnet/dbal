using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal
{
	public class Connection : IConnection
	{
		private readonly MiddlewareStack _middlewares = new MiddlewareStack();
		
		private readonly IDriver _driver;

		private readonly DbConnection _connection;

		private readonly IRowMapper _rowMapper;

		private readonly int? _defaultCommandTimeout;

		private readonly IsolationLevel _defaultTransactionIsolationLevel = IsolationLevel.Unspecified;

		private ITransaction? _transaction;

		public Connection(IDriver driver, IConfiguration? configuration = null)
		{
			_driver = driver;
			_connection = driver.ToDbConnection();
			_rowMapper = configuration?.RowMapper ?? new ActivatorRowMapper();

			if (configuration != null)
			{
				_defaultCommandTimeout = configuration.CommandTimeout;

				if (configuration.TransactionIsolation != null)
				{
					_defaultTransactionIsolationLevel = (IsolationLevel) configuration.TransactionIsolation;
				}
			}
		}

		public void Dispose()
		{
			_connection.Dispose();
		}

		public ValueTask DisposeAsync()
		{
			return _connection.DisposeAsync();
		}

		public IDriver ToDriver()
		{
			return _driver;
		}

		public async ValueTask Connect(CancellationToken cancellationToken = default)
		{
			if (_connection.State == ConnectionState.Open)
			{
				return;
			}
			
			await _connection.OpenAsync(cancellationToken);
			_transaction = null;
		}

		public async ValueTask Disconnect()
		{
			if (_connection.State == ConnectionState.Closed)
			{
				return;
			}

			await _connection.CloseAsync();
		}

		public async Task Reconnect(CancellationToken cancellationToken = default)
		{
			await Disconnect();
			await Connect(cancellationToken);
		}

		public async Task<ITransaction> BeginTransaction(CancellationToken cancellationToken = default)
		{
			await Connect(cancellationToken);

			if (_transaction == null)
			{
				return _transaction = await _driver.BeginTransaction(_defaultTransactionIsolationLevel, () => _transaction = null, cancellationToken);
			}

			if (_transaction != null && _transaction.SupportsSavepoints())
			{
				return await _transaction.CreateSavepoint($"DKX_DBAL_SAVEPOINT_{_transaction.SavepointsCount}", cancellationToken);
			}
			
			throw new NotSupportedException($"{_driver.GetType()} does not support nested transactions (savepoints)");
		}

		public async Task Transactional(Func<Task> fn, CancellationToken cancellationToken = default)
		{
			await using (var transaction = await BeginTransaction(cancellationToken))
			{
				try
				{
					await fn();
					await transaction.Commit(cancellationToken);
				}
				catch (Exception)
				{
					await transaction.Rollback(cancellationToken);
					throw;
				}
			}
		}

		public async Task<TResult> Transactional<TResult>(Func<Task<TResult>> fn, CancellationToken cancellationToken = default)
		{
			await using (var transaction = await BeginTransaction(cancellationToken))
			{
				try
				{
					var result = await fn();
					await transaction.Commit(cancellationToken);
					return result;
				}
				catch (Exception)
				{
					await transaction.Rollback(cancellationToken);
					throw;
				}
			}
		}

		public void AddMiddleware(IMiddleware middleware)
		{
			_middlewares.Add(middleware);
		}

		public IQueryBuilderFactory CreateQueryBuilder()
		{
			return new QueryBuilderFactory(this, _rowMapper);
		}

		public IQuery CreateQuery(string sql, CommandType commandType = CommandType.Text)
		{
			return new Query(this, _rowMapper, _middlewares, commandType, sql, null, _transaction, null, _defaultCommandTimeout);
		}

		public Task<int> Execute(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).Execute(cancellationToken);
		}

		public Task<TResult> ExecuteScalar<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToScalar<TResult>(cancellationToken);
		}

		public IAsyncEnumerable<TResult> QueryScalar<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToScalarResult<TResult>(cancellationToken);
		}

		public IAsyncEnumerable<TResult> Query<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToResult<TResult>(cancellationToken);
		}

		public IAsyncEnumerable<IImmutableDictionary<string, object?>> Query(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToResult(cancellationToken);
		}

		public Task<TResult?> QuerySingleOrDefault<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToSingleOrDefault<TResult>(cancellationToken);
		}

		public Task<IImmutableDictionary<string, object?>?> QuerySingleOrDefault(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToSingleOrDefault(cancellationToken);
		}

		public Task<TResult> QuerySingle<TResult>(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToSingle<TResult>(cancellationToken);
		}

		public Task<IImmutableDictionary<string, object?>> QuerySingle(string sql, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToSingle(cancellationToken);
		}

		public IAsyncEnumerable<TResult> QueryChunks<TResult>(string sql, int chunkSize, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
			where TResult : class
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToChunksResult<TResult>(chunkSize, cancellationToken);
		}

		public IAsyncEnumerable<IImmutableDictionary<string, object?>> QueryChunks(string sql, int chunkSize, object? parameters = null, CommandType commandType = CommandType.Text, ITransaction? transaction = null, string? name = null, CancellationToken cancellationToken = default)
		{
			return BuildQuery(commandType, sql, parameters, transaction, name).ToChunksResult(chunkSize, cancellationToken);
		}

		private IQuery BuildQuery(CommandType commandType, string sql, object? parameters, ITransaction? transaction = null, string? name = null)
		{
			var query = CreateQuery(sql, commandType);

			if (parameters != null)
			{
				query = query.WithParameters(ObjectParametersToDictionary(parameters));
			}

			if (transaction != null)
			{
				query = query.WithTransaction(transaction);
			}

			if (name != null)
			{
				query = query.WithName(name);
			}

			return query;
		}

		private static IImmutableDictionary<string, object?> ObjectParametersToDictionary(object parameters)
		{
			var result = ImmutableDictionary.Create<string, object?>();

			return parameters
				.GetType()
				.GetProperties(BindingFlags.Instance | BindingFlags.Public)
				.Aggregate(result, (current, property) => current.Add(property.Name, property.GetValue(parameters)));
		}
	}
}
