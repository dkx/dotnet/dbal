using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal
{
	public interface IQuery : ICloneable
	{
		CommandType CommandType { get; }
		
		string Sql { get; }
		
		IImmutableDictionary<string, object?>? Parameters { get; }
		
		ITransaction? Transaction { get; }
		
		string? Name { get; }
		
		int? Timeout { get; }

		IQuery WithRowMapper(IRowMapper rowMapper);

		IQuery WithParameters(IImmutableDictionary<string, object?>? parameters);

		IQuery WithParameter(string name, object? value);

		IQuery WithTransaction(ITransaction? transaction);

		IQuery WithName(string? name);

		IQuery WithTimeout(int? timeout);

		Task<int> Execute(CancellationToken cancellationToken = default);

		Task<TResult> ToScalar<TResult>(CancellationToken cancellationToken = default);

		IAsyncEnumerable<TResult> ToScalarResult<TResult>(CancellationToken cancellationToken = default);

		IAsyncEnumerable<TResult> ToResult<TResult>(CancellationToken cancellationToken = default)
			where TResult : class;

		IAsyncEnumerable<IImmutableDictionary<string, object?>> ToResult(CancellationToken cancellationToken = default);

		Task<TResult?> ToSingleOrDefault<TResult>(CancellationToken cancellationToken = default)
			where TResult : class;

		Task<IImmutableDictionary<string, object?>?> ToSingleOrDefault(CancellationToken cancellationToken = default);

		Task<TResult> ToSingle<TResult>(CancellationToken cancellationToken = default)
			where TResult : class;

		Task<IImmutableDictionary<string, object?>> ToSingle(CancellationToken cancellationToken = default);

		public IAsyncEnumerable<TResult> ToChunksResult<TResult>(int chunkSize, CancellationToken cancellationToken = default)
			where TResult : class;

		public IAsyncEnumerable<IImmutableDictionary<string, object?>> ToChunksResult(int chunkSize, CancellationToken cancellationToken = default);
	}
}
