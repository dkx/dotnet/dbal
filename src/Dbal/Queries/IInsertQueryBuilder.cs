using System.Collections.Generic;
using System.Collections.Immutable;

namespace DKX.Dbal.Queries
{
	public interface IInsertQueryBuilder : IQueryBuilder<IInsertQueryBuilder>
	{
		IInsertQueryBuilder Values(IImmutableDictionary<string, string> values);

		IInsertQueryBuilder Values(IDictionary<string, string> values);

		IInsertQueryBuilder SetValue(string column, string value);
	}
}
