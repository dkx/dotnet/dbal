namespace DKX.Dbal.Queries
{
	public interface IUpdateQueryBuilder : IQueryBuilder<IUpdateQueryBuilder>, IWhereQueryBuilder<IUpdateQueryBuilder>
	{
		IUpdateQueryBuilder Set(string key, string value);

		bool IsValid();
	}
}
