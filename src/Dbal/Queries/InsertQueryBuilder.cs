using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public class InsertQueryBuilder : BaseQueryBuilder<IInsertQueryBuilder>, IInsertQueryBuilder
	{
		private readonly IConnection _connection;

		private readonly FromClause _table;

		private readonly IImmutableDictionary<string, string> _values;
		
		internal InsertQueryBuilder(
			IConnection connection,
			IRowMapper rowMapper,
			FromClause table,
			IImmutableDictionary<string, string>? values = null,
			IImmutableDictionary<string, object?>? parameters = null
		) : base(connection, rowMapper, parameters)
		{
			_connection = connection;
			_table = table;
			_values = values ?? ImmutableSortedDictionary<string, string>.Empty;
		}

		public override IInsertQueryBuilder SetParameters(IImmutableDictionary<string, object?> parameters)
		{
			return new InsertQueryBuilder(_connection, RowMapper, _table, _values, parameters);
		}

		public override string ToSql()
		{
			var builder = new StringBuilder();

			builder.Append("INSERT INTO ").Append(_table.Table);
			builder.Append(" (").AppendJoin(", ", _values.Keys).Append(")");
			builder.Append(" VALUES (").AppendJoin(", ", _values.Values).Append(")");

			return builder.ToString();
		}

		public IInsertQueryBuilder Values(IImmutableDictionary<string, string> values)
		{
			return new InsertQueryBuilder(_connection, RowMapper, _table, values, Parameters);
		}

		public IInsertQueryBuilder Values(IDictionary<string, string> values)
		{
			return new InsertQueryBuilder(_connection, RowMapper, _table, ImmutableSortedDictionary.CreateRange(values), Parameters);
		}

		public IInsertQueryBuilder SetValue(string column, string value)
		{
			return new InsertQueryBuilder(_connection, RowMapper, _table, _values.SetItem(column, value), Parameters);
		}
	}
}
