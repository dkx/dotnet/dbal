namespace DKX.Dbal.Queries.Expressions
{
	public interface IExpression
	{
		string ToSql();
	}
}
