using System.Collections.Generic;

namespace DKX.Dbal.Queries.Expressions
{
	public interface IExpressionBuilder
	{
		string And(params string[] parts);

		string Or(params string[] parts);

		string Comparison(string leftExpr, string op, string rightExpr);

		string Equal(string leftExpr, string rightExpr);

		string NotEqual(string leftExpr, string rightExpr);

		string LowerThan(string leftExpr, string rightExpr);

		string LowerThanOrEqual(string leftExpr, string rightExpr);

		string GreaterThan(string leftExpr, string rightExpr);

		string GreaterThanOrEqual(string leftExpr, string rightExpr);

		string IsNull(string expr);

		string IsNotNull(string expr);

		string In(string expr, string group);

		string In(string expr, IEnumerable<string> group);

		string NotIn(string expr, string group);

		string NotIn(string expr, IEnumerable<string> group);

		string Like(string expr, string like, string? escape = null);

		string NotLike(string expr, string like, string? escape = null);
	}
}
