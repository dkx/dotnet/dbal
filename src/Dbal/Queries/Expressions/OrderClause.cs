namespace DKX.Dbal.Queries.Expressions
{
	public class OrderClause
	{
		public OrderClause(string sort, string order = "ASC")
		{
			Sort = sort;
			Order = order;
		}

		public string Sort { get; }

		public string Order { get; }
	}
}
