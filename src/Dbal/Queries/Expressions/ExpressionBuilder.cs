using System.Collections.Generic;
using System.Linq;

namespace DKX.Dbal.Queries.Expressions
{
	public class ExpressionBuilder : IExpressionBuilder
	{
		public string And(params string[] parts)
		{
			if (parts.Length == 1)
			{
				return parts[0];
			}
			
			return string.Join(" AND ", parts.Select(part => "(" + part + ")"));
		}
		
		public string Or(params string[] parts)
		{
			if (parts.Length == 1)
			{
				return parts[0];
			}
			
			return string.Join(" OR ", parts.Select(part => "(" + part + ")"));
		}

		public string Comparison(string leftExpr, string op, string rightExpr)
		{
			return $"{leftExpr} {op} {rightExpr}";
		}

		public string Equal(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, "=", rightExpr);
		}

		public string NotEqual(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, "<>", rightExpr);
		}

		public string LowerThan(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, "<", rightExpr);
		}

		public string LowerThanOrEqual(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, "<=", rightExpr);
		}

		public string GreaterThan(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, ">", rightExpr);
		}

		public string GreaterThanOrEqual(string leftExpr, string rightExpr)
		{
			return Comparison(leftExpr, ">=", rightExpr);
		}

		public string IsNull(string expr)
		{
			return $"{expr} IS NULL";
		}

		public string IsNotNull(string expr)
		{
			return $"{expr} IS NOT NULL";
		}

		public string In(string expr, string group)
		{
			return $"{expr} IN ({group})";
		}

		public string In(string expr, IEnumerable<string> group)
		{
			return In(expr, string.Join(", ", group));
		}

		public string NotIn(string expr, string group)
		{
			return $"{expr} NOT IN ({group})";
		}

		public string NotIn(string expr, IEnumerable<string> group)
		{
			return NotIn(expr, string.Join(", ", group));
		}

		public string Like(string expr, string like, string? escape = null)
		{
			return $"{expr} LIKE {like}" + (escape == null ? "" : $" ESCAPE {escape}");
		}

		public string NotLike(string expr, string like, string? escape = null)
		{
			return $"{expr} NOT LIKE {like}" + (escape == null ? "" : $" ESCAPE {escape}");
		}
	}
}
