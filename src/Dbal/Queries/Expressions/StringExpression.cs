namespace DKX.Dbal.Queries.Expressions
{
	public class StringExpression : IExpression
	{
		private readonly string _expression;

		public StringExpression(string expression)
		{
			_expression = expression;
		}
		
		public string ToSql()
		{
			return _expression;
		}
	}
}
