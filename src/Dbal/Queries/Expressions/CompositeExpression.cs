using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace DKX.Dbal.Queries.Expressions
{
	public class CompositeExpression : IExpression
	{
		private readonly IImmutableList<IExpression> _parts;
		
		public CompositeExpression(CompositeExpressionType type, IImmutableList<IExpression>? parts = null)
		{
			Type = type;
			_parts = parts ?? ImmutableList<IExpression>.Empty;
		}

		public CompositeExpression(CompositeExpressionType type, IExpression part)
			: this(type, ImmutableList.Create(part))
		{
		}
		
		public CompositeExpressionType Type { get; }

		public int Length => _parts.Count;

		public CompositeExpression Add(IExpression part)
		{
			return new CompositeExpression(Type, _parts.Add(part));
		}

		public CompositeExpression Add(IEnumerable<IExpression> parts)
		{
			return parts.Aggregate(this, (current, part) => current.Add(part));
		}

		public string ToSql()
		{
			if (Length == 1)
			{
				return _parts[0].ToSql();
			}

			return "(" + string.Join($") {Type.ToString().ToUpper()} (", _parts.Select(p => p.ToSql())) + ")";
		}

		public static CompositeExpression Merge(CompositeExpressionType type, IExpression add, CompositeExpression? into = null)
		{
			if (into != null && into.Type == type)
			{
				return into.Add(add);
			}

			var replace = new CompositeExpression(type);
			
			if (into != null)
			{
				replace = replace.Add(into);
			}

			return replace.Add(add);
		}
	}
}
