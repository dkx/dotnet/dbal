namespace DKX.Dbal.Queries.Expressions
{
	public class JoinClause
	{
		public JoinClause(JoinType type, string fromAlias, string table, string alias, string? condition = null)
		{
			Type = type;
			FromAlias = fromAlias;
			Table = table;
			Alias = alias;
			Condition = condition;
		}
		
		public JoinType Type { get; }

		public string FromAlias { get; }

		public string Table { get; }

		public string Alias { get; }
		
		public string? Condition { get; }
	}
}
