namespace DKX.Dbal.Queries.Expressions
{
	public class FromClause
	{
		public FromClause(string table, string? alias = null)
		{
			Table = table;
			Alias = alias;
		}
		
		public string Table { get; }
		
		public string? Alias { get; }

		public string TableSql => Alias == null ? Table : Table + " " + Alias;

		public string TableReference => Alias ?? Table;
	}
}
