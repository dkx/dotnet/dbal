using System.Collections.Immutable;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public interface ISelectQueryBuilder : IQueryBuilder<ISelectQueryBuilder>, IWhereQueryBuilder<ISelectQueryBuilder>
	{
		ISelectQueryBuilder WithRowMapper(IRowMapper rowMapper);
		
		ISelectQueryBuilder From(FromClause from);

		ISelectQueryBuilder From(string from, string? alias = null);

		ISelectQueryBuilder ClearFrom();
		
		ISelectQueryBuilder SetColumns(IImmutableList<string> columns);

		ISelectQueryBuilder SetColumns(params string[] columns);

		ISelectQueryBuilder AddColumns(params string[] columns);

		ISelectQueryBuilder Distinct();

		ISelectQueryBuilder Join(string fromAlias, string table, string alias, string? condition = null);

		ISelectQueryBuilder InnerJoin(string fromAlias, string table, string alias, string? condition = null);

		ISelectQueryBuilder LeftJoin(string fromAlias, string table, string alias, string? condition = null);

		ISelectQueryBuilder RightJoin(string fromAlias, string table, string alias, string? condition = null);

		ISelectQueryBuilder GroupBy(IImmutableList<string> groupBy);

		ISelectQueryBuilder GroupBy(params string[] groupBy);

		ISelectQueryBuilder AddGroupBy(params string[] groupBy);

		ISelectQueryBuilder Having(CompositeExpression having);

		ISelectQueryBuilder Having(IExpression having);

		ISelectQueryBuilder Having(string having);

		ISelectQueryBuilder AndHaving(IExpression having);

		ISelectQueryBuilder AndHaving(string having);

		ISelectQueryBuilder OrHaving(IExpression having);

		ISelectQueryBuilder OrHaving(string having);

		ISelectQueryBuilder OrderBy(IImmutableList<OrderClause> orderBy);

		ISelectQueryBuilder OrderBy(string sort, string order = "ASC");

		ISelectQueryBuilder AddOrderBy(string sort, string order = "ASC");

		ISelectQueryBuilder SetLimit(int? limit);

		ISelectQueryBuilder SetOffset(long? offset);
	}
}
