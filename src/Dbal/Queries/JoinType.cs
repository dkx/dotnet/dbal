namespace DKX.Dbal.Queries
{
	public enum JoinType
	{
		Inner,
		Left,
		Right,
	}
}
