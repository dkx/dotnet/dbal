using DKX.Dbal.Queries.Expressions;

namespace DKX.Dbal.Queries
{
	public interface IQueryBuilderFactory
	{
		IExpressionBuilder Expr { get; }
		
		ISelectQueryBuilder Select(params string[] columns);

		IUpdateQueryBuilder Update(string update, string? alias = null, object? values = null);

		IDeleteQueryBuilder Delete(string delete, string? alias = null);

		IInsertQueryBuilder Insert(string insert, object? values = null);
	}
}
