using System.Collections.Immutable;
using System.Reflection;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public class QueryBuilderFactory : IQueryBuilderFactory
	{
		private readonly IConnection _connection;

		private readonly IRowMapper _rowMapper;

		public QueryBuilderFactory(IConnection connection, IRowMapper rowMapper)
		{
			_connection = connection;
			_rowMapper = rowMapper;
		}
		
		public IExpressionBuilder Expr { get; } = new ExpressionBuilder();

		public ISelectQueryBuilder Select(params string[] columns)
		{
			return new SelectQueryBuilder(_connection, _rowMapper, columns.ToImmutableList());
		}

		public IUpdateQueryBuilder Update(string update, string? alias = null, object? values = null)
		{
			IUpdateQueryBuilder qb = new UpdateQueryBuilder(_connection, _rowMapper, new FromClause(update, alias));

			if (values != null)
			{
				var count = 0;
				foreach (var property in values.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
				{
					var propertyName = $"UpdateInit{count}";
					qb = qb.Set(property.Name, $"@{propertyName}");
					qb = qb.SetParameter(propertyName, property.GetValue(values));

					count++;
				}
			}

			return qb;
		}

		public IDeleteQueryBuilder Delete(string delete, string? alias = null)
		{
			return new DeleteQueryBuilder(_connection, _rowMapper, new FromClause(delete, alias));
		}

		public IInsertQueryBuilder Insert(string insert, object? values = null)
		{
			IInsertQueryBuilder qb = new InsertQueryBuilder(_connection, _rowMapper, new FromClause(insert));

			if (values != null)
			{
				var count = 0;
				foreach (var property in values.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
				{
					var propertyName = $"InsertInit{count}";
					qb = qb.SetValue(property.Name, $"@{propertyName}");
					qb = qb.SetParameter(propertyName, property.GetValue(values));

					count++;
				}
			}

			return qb;
		}
	}
}
