using System.Collections.Immutable;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public interface IQueryBuilder<out TClass>
	{
		IExpressionBuilder Expr { get; }
		
		IRowMapper RowMapper { get; }
		
		TClass SetParameters(IImmutableDictionary<string, object?> parameters);

		TClass SetParameter(string name, object? value);
		
		string ToSql();

		IQuery ToQuery();
	}
}
