using System.Collections.Immutable;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public abstract class BaseQueryBuilder<TClass>
	{
		private readonly IConnection _connection;

		protected readonly IImmutableDictionary<string, object?> Parameters;
		
		protected BaseQueryBuilder(
			IConnection connection,
			IRowMapper rowMapper,
			IImmutableDictionary<string, object?>? parameters = null
		)
		{
			_connection = connection;
			Parameters = parameters ?? ImmutableDictionary<string, object?>.Empty;
			RowMapper = rowMapper;
		}

		public IExpressionBuilder Expr { get; } = new ExpressionBuilder();

		public IRowMapper RowMapper { get; }

		public abstract TClass SetParameters(IImmutableDictionary<string, object?> parameters);

		public abstract string ToSql();

		public TClass SetParameter(string name, object? value)
		{
			return SetParameters(Parameters.Add(name, value));
		}
		
		public IQuery ToQuery()
		{
			var query = _connection
				.CreateQuery(ToSql())
				.WithRowMapper(RowMapper);

			if (Parameters.Count > 0)
			{
				query = query.WithParameters(Parameters);
			}

			return query;
		}
	}
}
