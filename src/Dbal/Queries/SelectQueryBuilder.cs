using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public class SelectQueryBuilder : BaseQueryBuilderWithWhere<ISelectQueryBuilder>, ISelectQueryBuilder
	{
		private readonly IConnection _connection;
		
		private readonly IImmutableList<string> _columns;

		private readonly IImmutableList<FromClause> _from;

		private readonly bool _distinct;

		private readonly IImmutableDictionary<string, IImmutableList<JoinClause>> _join;

		private readonly IImmutableList<string> _groupBy;

		private readonly CompositeExpression? _having;

		private readonly IImmutableList<OrderClause> _orderBy;

		private readonly int? _limit;

		private readonly long? _offset;
		
		public SelectQueryBuilder(
			IConnection connection,
			IRowMapper rowMapper,
			IImmutableList<string> columns,
			IImmutableList<FromClause>? from = null,
			bool distinct = false,
			IImmutableDictionary<string, IImmutableList<JoinClause>>? join = null,
			IImmutableList<string>? groupBy = null,
			CompositeExpression? having = null,
			IImmutableList<OrderClause>? order = null,
			int? limit = null,
			long? offset = null,
			CompositeExpression? where = null,
			IImmutableDictionary<string, object?>? parameters = null
		) : base(connection, rowMapper, where, parameters)
		{
			_connection = connection;
			_columns = columns.ToImmutableList();
			_from = from ?? ImmutableList<FromClause>.Empty;
			_distinct = distinct;
			_join = join ?? ImmutableDictionary<string, IImmutableList<JoinClause>>.Empty;
			_groupBy = groupBy ?? ImmutableList<string>.Empty;
			_having = having;
			_orderBy = order ?? ImmutableList<OrderClause>.Empty;
			_limit = limit;
			_offset = offset;
		}

		public ISelectQueryBuilder WithRowMapper(IRowMapper rowMapper)
		{
			return new SelectQueryBuilder(_connection, rowMapper, _columns, _from, _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder From(FromClause from)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from.Add(from), _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder From(string from, string? alias = null)
		{
			return From(new FromClause(from, alias));
		}

		public ISelectQueryBuilder ClearFrom()
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, ImmutableList<FromClause>.Empty, _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder SetColumns(IImmutableList<string> columns)
		{
			return new SelectQueryBuilder(_connection, RowMapper, columns, _from, _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder SetColumns(params string[] columns)
		{
			return SetColumns(columns.ToImmutableList());
		}

		public ISelectQueryBuilder AddColumns(params string[] columns)
		{
			return SetColumns(_columns.AddRange(columns));
		}
		
		public ISelectQueryBuilder Distinct()
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, true, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder Join(string fromAlias, string table, string alias, string? condition = null)
		{
			return InnerJoin(fromAlias, table, alias, condition);
		}

		public ISelectQueryBuilder InnerJoin(string fromAlias, string table, string alias, string? condition = null)
		{
			return AddJoin(new JoinClause(JoinType.Inner, fromAlias, table, alias, condition));
		}

		public ISelectQueryBuilder LeftJoin(string fromAlias, string table, string alias, string? condition = null)
		{
			return AddJoin(new JoinClause(JoinType.Left, fromAlias, table, alias, condition));
		}

		public ISelectQueryBuilder RightJoin(string fromAlias, string table, string alias, string? condition = null)
		{
			return AddJoin(new JoinClause(JoinType.Right, fromAlias, table, alias, condition));
		}

		public ISelectQueryBuilder GroupBy(IImmutableList<string> groupBy)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder GroupBy(params string[] groupBy)
		{
			return GroupBy(groupBy.ToImmutableList());
		}

		public ISelectQueryBuilder AddGroupBy(params string[] groupBy)
		{
			return GroupBy(_groupBy.AddRange(groupBy));
		}

		public ISelectQueryBuilder Having(CompositeExpression having)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder Having(IExpression having)
		{
			return having is CompositeExpression expr
				? Having(expr)
				: Having(new CompositeExpression(CompositeExpressionType.And, having));
		}

		public ISelectQueryBuilder Having(string having)
		{
			return Having(new StringExpression(having));
		}

		public ISelectQueryBuilder AndHaving(IExpression having)
		{
			return Having(CompositeExpression.Merge(CompositeExpressionType.And, having, _having));
		}

		public ISelectQueryBuilder AndHaving(string having)
		{
			return AndHaving(new StringExpression(having));
		}

		public ISelectQueryBuilder OrHaving(IExpression having)
		{
			return Having(CompositeExpression.Merge(CompositeExpressionType.Or, having, _having));
		}

		public ISelectQueryBuilder OrHaving(string having)
		{
			return OrHaving(new StringExpression(having));
		}

		public ISelectQueryBuilder OrderBy(IImmutableList<OrderClause> orderBy)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, _having, orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder OrderBy(string sort, string order = "ASC")
		{
			return OrderBy(ImmutableList.Create(new OrderClause(sort, order)));
		}

		public ISelectQueryBuilder AddOrderBy(string sort, string order = "ASC")
		{
			return OrderBy(_orderBy.Add(new OrderClause(sort, order)));
		}

		public ISelectQueryBuilder SetLimit(int? limit)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, _having, _orderBy, limit, _offset, WhereExpr, Parameters);
		}

		public ISelectQueryBuilder SetOffset(long? offset)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, _having, _orderBy, _limit, offset, WhereExpr, Parameters);
		}

		public override ISelectQueryBuilder Where(CompositeExpression where)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, where, Parameters);
		}

		public override ISelectQueryBuilder SetParameters(IImmutableDictionary<string, object?> parameters)
		{
			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, _join, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, parameters);
		}

		public override string ToSql()
		{
			var builder = new StringBuilder();

			builder.Append("SELECT ").Append(_distinct ? "DISTINCT " : "").AppendJoin(", ", _columns);

			if (_from.Count > 0)
			{
				builder.Append(" FROM ").AppendJoin(", ", GetFromClauses());
			}

			if (WhereExpr != null)
			{
				builder.Append(" WHERE ").Append(WhereExpr.ToSql());
			}

			if (_groupBy.Count > 0)
			{
				builder.Append(" GROUP BY ").AppendJoin(", ", _groupBy);
			}

			if (_having != null)
			{
				builder.Append(" HAVING ").Append(_having.ToSql());
			}

			if (_orderBy.Count > 0)
			{
				builder.Append(" ORDER BY ").AppendJoin(", ", _orderBy.Select(o => o.Sort + " " + o.Order));
			}

			var sql = builder.ToString();

			if (_limit != null || _offset != null)
			{
				return _connection.ToDriver().ModifyLimitQuery(sql, _limit, _offset);
			}

			return sql;
		}

		private ISelectQueryBuilder AddJoin(JoinClause join)
		{
			var list = _join.ContainsKey(join.FromAlias)
				? _join[join.FromAlias]
				: ImmutableList<JoinClause>.Empty;

			var joins = _join.SetItem(join.FromAlias, list.Add(join));

			return new SelectQueryBuilder(_connection, RowMapper, _columns, _from, _distinct, joins, _groupBy, _having, _orderBy, _limit, _offset, WhereExpr, Parameters);
		}

		private IEnumerable<string> GetFromClauses()
		{
			var fromClauses = new Dictionary<string, string>();
			var knownAliases = new Dictionary<string, bool>();

			foreach (var from in _from)
			{
				knownAliases.Add(from.TableReference, true);
				fromClauses.Add(from.TableReference, from.TableSql + GetSqlForJoins(from.TableReference, knownAliases));
			}

			return fromClauses.ToArray().Select(f => f.Value);
		}

		private string GetSqlForJoins(string fromAlias, IDictionary<string, bool> knownAliases)
		{
			if (!_join.ContainsKey(fromAlias))
			{
				return "";
			}
			
			var builder = new StringBuilder();

			foreach (var join in _join[fromAlias])
			{
				if (knownAliases.ContainsKey(join.Alias))
				{
					throw new QueryException($"The given alias '{join.Alias}' is not unique in FROM and JOIN clause table. The currently registered aliases are: {string.Join(", ", knownAliases.ToArray().Select(a => a.Key))}.");
				}

				builder.Append(" ").Append(join.Type.ToString().ToUpper()).Append(" JOIN ").Append(join.Table).Append(" ").Append(join.Alias);

				if (join.Condition != null)
				{
					builder.Append(" ON ").Append(join.Condition);
				}

				knownAliases[join.Alias] = true;
			}

			foreach (var join in _join[fromAlias])
			{
				builder.Append(GetSqlForJoins(join.Alias, knownAliases));
			}

			return builder.ToString();
		}
	}
}
