using System.Collections.Immutable;
using System.Linq;
using System.Text;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public class UpdateQueryBuilder : BaseQueryBuilderWithWhere<IUpdateQueryBuilder>, IUpdateQueryBuilder
	{
		private readonly IConnection _connection;
		
		private readonly FromClause _table;
		
		private readonly IImmutableDictionary<string, string> _set;

		public UpdateQueryBuilder(
			IConnection connection,
			IRowMapper rowMapper,
			FromClause table,
			IImmutableDictionary<string, string>? set = null,
			CompositeExpression? where = null,
			IImmutableDictionary<string, object?>? parameters = null
		) : base(connection, rowMapper, where, parameters)
		{
			_connection = connection;
			_table = table;
			_set = set ?? ImmutableSortedDictionary<string, string>.Empty;
		}

		public IUpdateQueryBuilder Set(string key, string value)
		{
			return new UpdateQueryBuilder(_connection, RowMapper, _table, _set.Add(key, value), WhereExpr, Parameters);
		}

		public bool IsValid()
		{
			return _set.Count > 0;
		}

		public override IUpdateQueryBuilder Where(CompositeExpression where)
		{
			return new UpdateQueryBuilder(_connection, RowMapper, _table, _set, where, Parameters);
		}

		public override IUpdateQueryBuilder SetParameters(IImmutableDictionary<string, object?> parameters)
		{
			return new UpdateQueryBuilder(_connection, RowMapper, _table, _set, WhereExpr, parameters);
		}

		public override string ToSql()
		{
			var builder = new StringBuilder();

			builder.Append("UPDATE ").Append(_table.TableSql);
			builder.Append(" SET ").AppendJoin(", ", _set.Select(s => s.Key + " = " + s.Value));

			if (WhereExpr != null)
			{
				builder.Append(" WHERE ").Append(WhereExpr.ToSql());
			}

			return builder.ToString();
		}
	}
}
