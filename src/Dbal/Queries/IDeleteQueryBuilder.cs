namespace DKX.Dbal.Queries
{
	public interface IDeleteQueryBuilder : IQueryBuilder<IDeleteQueryBuilder>, IWhereQueryBuilder<IDeleteQueryBuilder>
	{
	}
}
