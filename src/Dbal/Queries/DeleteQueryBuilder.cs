using System.Collections.Immutable;
using System.Text;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public class DeleteQueryBuilder : BaseQueryBuilderWithWhere<IDeleteQueryBuilder>, IDeleteQueryBuilder
	{
		private readonly IConnection _connection;

		private readonly FromClause _table;
		
		public DeleteQueryBuilder(
			IConnection connection,
			IRowMapper rowMapper,
			FromClause table,
			CompositeExpression? where = null,
			IImmutableDictionary<string, object?>? parameters = null
		) : base(connection, rowMapper, where, parameters)
		{
			_connection = connection;
			_table = table;
		}

		public override IDeleteQueryBuilder Where(CompositeExpression where)
		{
			return new DeleteQueryBuilder(_connection, RowMapper, _table, where, Parameters);
		}

		public override IDeleteQueryBuilder SetParameters(IImmutableDictionary<string, object?> parameters)
		{
			return new DeleteQueryBuilder(_connection, RowMapper, _table, WhereExpr, parameters);
		}

		public override string ToSql()
		{
			var builder = new StringBuilder();

			builder.Append("DELETE FROM ").Append(_table.TableSql);

			if (WhereExpr != null)
			{
				builder.Append(" WHERE ").Append(WhereExpr.ToSql());
			}

			return builder.ToString();
		}
	}
}
