using DKX.Dbal.Queries.Expressions;

namespace DKX.Dbal.Queries
{
	public interface IWhereQueryBuilder<out TClass>
	{
		TClass Where(CompositeExpression where);

		TClass Where(IExpression where);

		TClass Where(string where);

		TClass AndWhere(IExpression where);

		TClass AndWhere(string where);

		TClass OrWhere(IExpression where);

		TClass OrWhere(string where);
	}
}
