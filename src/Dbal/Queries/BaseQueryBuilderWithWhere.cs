using System.Collections.Immutable;
using DKX.Dbal.Queries.Expressions;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal.Queries
{
	public abstract class BaseQueryBuilderWithWhere<TClass> : BaseQueryBuilder<TClass>
	{
		protected readonly CompositeExpression? WhereExpr;
		
		protected BaseQueryBuilderWithWhere(
			IConnection connection,
			IRowMapper rowMapper,
			CompositeExpression? where,
			IImmutableDictionary<string, object?>? parameters = null
		) : base(connection, rowMapper, parameters)
		{
			WhereExpr = where;
		}

		public abstract TClass Where(CompositeExpression where);

		public TClass Where(IExpression where)
		{
			return where is CompositeExpression expr
				? Where(expr)
				: Where(new CompositeExpression(CompositeExpressionType.And, where));
		}

		public TClass Where(string where)
		{
			return Where(new StringExpression(where));
		}

		public TClass AndWhere(IExpression where)
		{
			return Where(CompositeExpression.Merge(CompositeExpressionType.And, where, WhereExpr));
		}

		public TClass AndWhere(string where)
		{
			return AndWhere(new StringExpression(where));
		}

		public TClass OrWhere(IExpression where)
		{
			return Where(CompositeExpression.Merge(CompositeExpressionType.Or, where, WhereExpr));
		}

		public TClass OrWhere(string where)
		{
			return OrWhere(new StringExpression(where));
		}
	}
}
