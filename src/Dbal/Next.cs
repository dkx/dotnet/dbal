using System.Threading.Tasks;

namespace DKX.Dbal
{
	public delegate Task<TResult> Next<TResult>();
}
