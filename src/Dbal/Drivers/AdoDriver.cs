using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal.Drivers
{
	public class AdoDriver : IDriver
	{
		private readonly DbConnection _connection;
		
		public AdoDriver(DbConnection connection)
		{
			_connection = connection;
		}

		public DbConnection ToDbConnection()
		{
			return _connection;
		}

		public virtual async Task<ITransaction> BeginTransaction(IsolationLevel isolationLevel, Action closed, CancellationToken cancellationToken = default)
		{
			return new Transaction(await _connection.BeginTransactionAsync(isolationLevel, cancellationToken), closed);
		}

		public virtual string ModifyLimitQuery(string query, int? limit, long? offset)
		{
			offset ??= 0;

			if (offset < 0)
			{
				throw new QueryException($"Offset must be a positive integer or zero, {offset} given");
			}

			if (offset > 0 && !SupportsLimitOffset())
			{
				throw new NotSupportedException("AdoDriver does not support offset values in limit queries");
			}

			return DoModifyLimitQuery(query, limit, (long) offset);
		}

		public bool SupportsLimitOffset()
		{
			return true;
		}

		protected virtual string DoModifyLimitQuery(string query, int? limit, long offset)
		{
			if (limit != null)
			{
				query += " LIMIT " + limit;
			}

			if (offset > 0)
			{
				query += " OFFSET " + offset;
			}

			return query;
		}
	}
}
