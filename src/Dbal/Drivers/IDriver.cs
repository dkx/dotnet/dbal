using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal.Drivers
{
	public interface IDriver
	{
		DbConnection ToDbConnection();

		Task<ITransaction> BeginTransaction(IsolationLevel isolationLevel, Action closed, CancellationToken cancellationToken = default);

		string ModifyLimitQuery(string query, int? limit, long? offset);

		bool SupportsLimitOffset();
	}
}
