using System;

namespace DKX.Dbal
{
	public class DbalException : Exception
	{
		public DbalException(string message) : base(message)
		{
		}
	}
}
