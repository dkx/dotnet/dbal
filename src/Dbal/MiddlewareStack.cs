using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal
{
	public class MiddlewareStack : IMiddlewareStack
	{
		private readonly IList<IMiddleware> _middlewares = new List<IMiddleware>();
		
		public void Add(IMiddleware middleware)
		{
			_middlewares.Add(middleware);
		}

		public async Task<TResult> Run<TResult>(IQuery query, Next<TResult> run, CancellationToken cancellationToken)
		{
			if (_middlewares.Count < 1)
			{
				return await run();
			}

			var next = CreateNext(query, 0, run, cancellationToken);

			return await _middlewares[0].Handle(query, next, cancellationToken);
		}

		private Next<TResult> CreateNext<TResult>(IQuery query, int current, Next<TResult> lastRun, CancellationToken cancellationToken)
		{
			if (current >= _middlewares.Count - 1)
			{
				return lastRun;
			}

			var next = _middlewares[current + 1];

			return () => next.Handle(query, CreateNext(query, ++current, lastRun, cancellationToken), cancellationToken);
		}
	}
}
