using System;

namespace DKX.Dbal
{
	public class QueryException : DbalException
	{
		public QueryException(string message) : base(message)
		{
		}
	}
}
