using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal
{
	public class Transaction : ITransaction
	{
		private readonly DbTransaction _transaction;

		private readonly Action _closed;
		
		public Transaction(DbTransaction transaction, Action closed)
		{
			_transaction = transaction;
			_closed = closed;
		}

		public async ValueTask DisposeAsync()
		{
			await _transaction.DisposeAsync();
			_closed();
		}

		public uint SavepointsCount => 0;

		public async Task Commit(CancellationToken cancellationToken = default)
		{
			await _transaction.CommitAsync(cancellationToken);
			_closed();
		}

		public async Task Rollback(CancellationToken cancellationToken = default)
		{
			await _transaction.RollbackAsync(cancellationToken);
			_closed();
		}

		public bool SupportsSavepoints()
		{
			return false;
		}

		public Task<ISavepoint> CreateSavepoint(string name, CancellationToken cancellationToken = default)
		{
			throw new NotSupportedException("Default AdoDriver does not support savepoints");
		}

		public DbTransaction ToDbTransaction()
		{
			return _transaction;
		}
	}
}
