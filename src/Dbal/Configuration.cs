using System.Data;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal
{
	public class Configuration : IConfiguration
	{
		public int? CommandTimeout { get; set; }
		
		public IsolationLevel? TransactionIsolation { get; set; }

		public IRowMapper? RowMapper { get; set; }
	}
}
