using System;
using System.Data;
using System.Linq;

namespace DKX.Dbal.RowMapping
{
	public sealed class ActivatorRowMapper : IRowMapper
	{
		public TResult Map<TResult>(Type resultType, IDataRecord record)
			where TResult : class
		{
			object[] values = new object[record.FieldCount];
			record.GetValues(values);

			object?[] nullableValues = values.Select(v => v is DBNull ? null : v).ToArray();
			
			try
			{
				return (Activator.CreateInstance(resultType, args: nullableValues) as TResult)!;
			}
			catch (MissingMemberException)
			{
				var fields = values.Select(value => value == null ? "null" : value.GetType().ToString());

				throw new DbalException($"{resultType} does not have any suitable constructor for [{string.Join(", ", fields)}]");
			}
		}
	}
}
