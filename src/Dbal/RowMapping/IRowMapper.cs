using System;
using System.Data;

namespace DKX.Dbal.RowMapping
{
	public interface IRowMapper
	{
		TResult Map<TResult>(Type resultType, IDataRecord record)
			where TResult : class;
	}
}
