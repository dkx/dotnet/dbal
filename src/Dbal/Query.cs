using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Data.Common;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal
{
	public class Query : IQuery
	{
		private readonly IConnection _connection;

		private readonly IRowMapper _rowMapper;

		private readonly IMiddlewareStack _middlewares;
		
		public Query(
			IConnection connection,
			IRowMapper rowMapper,
			IMiddlewareStack middlewares,
			CommandType commandType,
			string sql,
			IImmutableDictionary<string, object?>? parameters,
			ITransaction? transaction,
			string? name,
			int? timeout
		)
		{
			_connection = connection;
			_rowMapper = rowMapper;
			_middlewares = middlewares;
			CommandType = commandType;
			Sql = sql;
			Parameters = parameters;
			Transaction = transaction;
			Name = name;
			Timeout = timeout;
		}
		
		public CommandType CommandType { get; }
		
		public string Sql { get; }

		public IImmutableDictionary<string, object?>? Parameters { get; }

		public ITransaction? Transaction { get; }
		
		public string? Name { get; }
		
		public int? Timeout { get; }

		public object Clone()
		{
			return new Query(_connection, _rowMapper, _middlewares, CommandType, Sql, Parameters, Transaction, Name, Timeout);
		}

		public IQuery WithRowMapper(IRowMapper rowMapper)
		{
			return new Query(_connection, rowMapper, _middlewares, CommandType, Sql, Parameters, Transaction, Name, Timeout);
		}

		public IQuery WithParameters(IImmutableDictionary<string, object?>? parameters)
		{
			return new Query(_connection, _rowMapper, _middlewares, CommandType, Sql, parameters, Transaction, Name, Timeout);
		}

		public IQuery WithParameter(string name, object? value)
		{
			var parameters = Parameters ?? ImmutableDictionary<string, object?>.Empty;
			parameters = parameters.Add(name, value);

			return WithParameters(parameters);
		}

		public IQuery WithTransaction(ITransaction? transaction)
		{
			return new Query(_connection, _rowMapper, _middlewares, CommandType, Sql, Parameters, transaction, Name, Timeout);
		}

		public IQuery WithName(string? name)
		{
			return new Query(_connection, _rowMapper, _middlewares, CommandType, Sql, Parameters, Transaction, name, Timeout);
		}

		public IQuery WithTimeout(int? timeout)
		{
			return new Query(_connection, _rowMapper, _middlewares, CommandType, Sql, Parameters, Transaction, Name, timeout);
		}

		public async Task<int> Execute(CancellationToken cancellationToken = default)
		{
			await using var command = await CreateCommand(cancellationToken);
			return await _middlewares.Run(this, () => command.ExecuteNonQueryAsync(cancellationToken), cancellationToken);
		}

		public async Task<TResult> ToScalar<TResult>(CancellationToken cancellationToken = default)
		{
			await using var command = await CreateCommand(cancellationToken);
			return await _middlewares.Run(this, async () =>
			{
				var result = await command.ExecuteScalarAsync(cancellationToken);
				if (result is DBNull)
				{
					return default!;
				}

				return (TResult) result;
			}, cancellationToken);
		}

		public async IAsyncEnumerable<TResult> ToScalarResult<TResult>([EnumeratorCancellation] CancellationToken cancellationToken = default)
		{
			await using var command = await CreateCommand(cancellationToken);
			await using var reader = await _middlewares.Run(this, () => command.ExecuteReaderAsync(cancellationToken), cancellationToken);

			while (await reader.ReadAsync(cancellationToken))
			{
				if (reader.FieldCount != 1)
				{
					throw new DbalException($"ToScalarResult: query '{command.CommandText}' returns more than one columns");
				}

				var value = reader.GetValue(0);
				if (value is DBNull)
				{
					yield return default!;
				}

				yield return (TResult) value;
			}
		}

		public async IAsyncEnumerable<TResult> ToResult<TResult>([EnumeratorCancellation] CancellationToken cancellationToken = default) where TResult : class
		{
			var resultType = typeof(TResult);
			await using var command = await CreateCommand(cancellationToken);
			await using var reader = await _middlewares.Run(this, () => command.ExecuteReaderAsync(cancellationToken), cancellationToken);
			
			while (await reader.ReadAsync(cancellationToken))
			{
				yield return _rowMapper.Map<TResult>(resultType, reader);
			}
		}

		public async IAsyncEnumerable<IImmutableDictionary<string, object?>> ToResult([EnumeratorCancellation] CancellationToken cancellationToken = default)
		{
			await using var command = await CreateCommand(cancellationToken);
			await using var reader = await _middlewares.Run(this, () => command.ExecuteReaderAsync(cancellationToken), cancellationToken);
			
			while (await reader.ReadAsync(cancellationToken))
			{
				yield return MapDictionary(reader);
			}
		}

		public async Task<TResult?> ToSingleOrDefault<TResult>(CancellationToken cancellationToken = default) where TResult : class
		{
			var resultType = typeof(TResult);
			await using var command = await CreateCommand(cancellationToken);
			await using var reader = await _middlewares.Run(this, () => command.ExecuteReaderAsync(cancellationToken), cancellationToken);

			TResult? item = null;
			while (await reader.ReadAsync(cancellationToken))
			{
				if (item != null)
				{
					await reader.CloseAsync();
					throw new DbalException($"Query must return only one row ({Sql})");
				}

				item = _rowMapper.Map<TResult>(resultType, reader);
			}

			return item;
		}

		public async Task<IImmutableDictionary<string, object?>?> ToSingleOrDefault(CancellationToken cancellationToken = default)
		{
			await using var command = await CreateCommand(cancellationToken);
			await using var reader = await _middlewares.Run(this, () => command.ExecuteReaderAsync(cancellationToken), cancellationToken);

			IImmutableDictionary<string, object?>? item = null;
			while (await reader.ReadAsync(cancellationToken))
			{
				if (item != null)
				{
					await reader.CloseAsync();
					throw new DbalException($"Query must return only one row ({Sql})");
				}

				item = MapDictionary(reader);
			}

			return item;
		}

		public async Task<TResult> ToSingle<TResult>(CancellationToken cancellationToken = default) where TResult : class
		{
			var item = await ToSingleOrDefault<TResult>(cancellationToken);
			if (item == null)
			{
				throw new DbalException($"Query must return exactly one row but returned zero ({Sql})");
			}

			return item;
		}

		public async Task<IImmutableDictionary<string, object?>> ToSingle(CancellationToken cancellationToken = default)
		{
			var item = await ToSingleOrDefault(cancellationToken);
			if (item == null)
			{
				throw new DbalException($"Query must return exactly one row but returned zero ({Sql})");
			}

			return item;
		}

		public async IAsyncEnumerable<TResult> ToChunksResult<TResult>(int chunkSize, [EnumeratorCancellation] CancellationToken cancellationToken = default) where TResult : class
		{
			var offset = 0;
			var driver = _connection.ToDriver();
			
			while (true)
			{
				var empty = true;
				var sql = driver.ModifyLimitQuery(Sql, chunkSize, offset);
				var query = new Query(_connection, _rowMapper, _middlewares, CommandType, sql, Parameters, Transaction, Name, Timeout);
				
				await foreach (var item in query.ToResult<TResult>(cancellationToken))
				{
					yield return item;
					offset++;
					empty = false;
				}

				if (empty)
				{
					break;
				}
			}
		}

		public async IAsyncEnumerable<IImmutableDictionary<string, object?>> ToChunksResult(int chunkSize, [EnumeratorCancellation] CancellationToken cancellationToken = default)
		{
			var offset = 0;
			var driver = _connection.ToDriver();
			
			while (true)
			{
				var empty = true;
				var sql = driver.ModifyLimitQuery(Sql, chunkSize, offset);
				var query = new Query(_connection, _rowMapper, _middlewares, CommandType, sql, Parameters, Transaction, Name, Timeout);
				
				await foreach (var item in query.ToResult(cancellationToken))
				{
					yield return item;
					offset++;
					empty = false;
				}

				if (empty)
				{
					break;
				}
			}
		}

		private async Task<DbCommand> CreateCommand(CancellationToken cancellationToken)
		{
			var command = _connection.ToDriver().ToDbConnection().CreateCommand();
			command.CommandType = CommandType;
			command.CommandText = Sql;

			if (Timeout != null)
			{
				command.CommandTimeout = (int) Timeout;
			}

			await _connection.Connect(cancellationToken);

			if (Transaction != null)
			{
				command.Transaction = Transaction.ToDbTransaction();
			}

			if (Parameters != null)
			{
				foreach (var (name, value) in Parameters)
				{
					var parameter = command.CreateParameter();
					parameter.ParameterName = name;
					parameter.Value = value ?? DBNull.Value;

					command.Parameters.Add(parameter);
				}
				
				await command.PrepareAsync(cancellationToken);
			}

			return command;
		}

		private static IImmutableDictionary<string, object?> MapDictionary(IDataRecord reader)
		{
			var result = ImmutableDictionary<string, object?>.Empty;
			for (var i = 0; i < reader.FieldCount; i++)
			{
				result = result.Add(
					reader.GetName(i),
					reader.IsDBNull(i) ? null : reader.GetValue(i)
				);
			}

			return result;
		}
	}
}
