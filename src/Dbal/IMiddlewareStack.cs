using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal
{
	public interface IMiddlewareStack
	{
		void Add(IMiddleware middleware);

		Task<TResult> Run<TResult>(IQuery query, Next<TResult> run, CancellationToken cancellationToken);
	}
}
