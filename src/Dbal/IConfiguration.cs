using System.Data;
using DKX.Dbal.RowMapping;

namespace DKX.Dbal
{
	public interface IConfiguration
	{
		int? CommandTimeout { get; }
		
		IsolationLevel? TransactionIsolation { get; }
		
		IRowMapper? RowMapper { get; }
	}
}
