using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal
{
	public interface IMiddleware
	{
		Task<TResult> Handle<TResult>(IQuery query, Next<TResult> next, CancellationToken cancellationToken);
	}
}
