using System;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace DKX.Dbal
{
	public interface ITransaction : IAsyncDisposable
	{
		public uint SavepointsCount { get; }
		
		Task Commit(CancellationToken cancellationToken = default);

		Task Rollback(CancellationToken cancellationToken = default);

		bool SupportsSavepoints();

		Task<ISavepoint> CreateSavepoint(string name, CancellationToken cancellationToken = default);

		DbTransaction ToDbTransaction();
	}
}
