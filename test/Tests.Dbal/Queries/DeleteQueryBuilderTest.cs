using System.Data.Common;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;
using Moq;
using Xunit;

namespace DKX.DbalTests.Queries
{
	public class DeleteQueryBuilderTest
	{
		private readonly IQueryBuilderFactory _factory;

		public DeleteQueryBuilderTest()
		{
			var driver = new AdoDriver(new Mock<DbConnection>().Object);
			var connection = new Mock<IConnection>();
			connection.Setup(c => c.ToDriver()).Returns(driver);

			_factory = new QueryBuilderFactory(connection.Object, new ActivatorRowMapper());
		}

		[Fact]
		public void ToString_Delete()
		{
			var qb = _factory.Delete("users", "u");

			Assert.Equal("DELETE FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToString_DeleteWithoutAlias()
		{
			var qb = _factory.Delete("users");

			Assert.Equal("DELETE FROM users", qb.ToSql());
		}

		[Fact]
		public void ToString_DeleteWhere()
		{
			var qb = _factory
				.Delete("users", "u")
				.Where("u.foo = @Foo");

			Assert.Equal("DELETE FROM users u WHERE u.foo = @Foo", qb.ToSql());
		}
	}
}
