using System.Data.Common;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;
using Moq;
using Xunit;

namespace DKX.DbalTests.Queries
{
	public class SelectQueryBuilderTest
	{
		private readonly IQueryBuilderFactory _factory;

		public SelectQueryBuilderTest()
		{
			var driver = new AdoDriver(new Mock<DbConnection>().Object);
			var connection = new Mock<IConnection>();
			connection.Setup(c => c.ToDriver()).Returns(driver);

			_factory = new QueryBuilderFactory(connection.Object, new ActivatorRowMapper());
		}

		[Fact]
		public void ToSql_SimpleSelectWithoutFrom()
		{
			var qb = _factory.Select("some_function()");

			Assert.Equal("SELECT some_function()", qb.ToSql());
		}

		[Fact]
		public void ToSql_SimpleSelect()
		{
			var qb = _factory
				.Select("u.id")
				.From("users", "u");

			Assert.Equal("SELECT u.id FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToSql_SimpleSelectWithDistinct()
		{
			var qb = _factory
				.Select("u.id")
				.Distinct()
				.From("users", "u");

			Assert.Equal("SELECT DISTINCT u.id FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithSimpleWhere()
		{
			var qb = _factory
				.Select("u.id")
				.From("users", "u")
				.Where(_factory.Expr.And(
					_factory.Expr.Equal("u.nickname", "?")
				));

			Assert.Equal("SELECT u.id FROM users u WHERE u.nickname = ?", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithLeftJoin()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.LeftJoin("u", "phones", "p", _factory.Expr.Equal("p.user_id", "u.id"));

			Assert.Equal("SELECT u.*, p.* FROM users u LEFT JOIN phones p ON p.user_id = u.id", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithJoin()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.Join("u", "phones", "p", _factory.Expr.Equal("p.user_id", "u.id"));

			Assert.Equal("SELECT u.*, p.* FROM users u INNER JOIN phones p ON p.user_id = u.id", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithJoinNoCondition()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.Join("u", "phones", "p");

			Assert.Equal("SELECT u.*, p.* FROM users u INNER JOIN phones p", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithInnerJoin()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.InnerJoin("u", "phones", "p", _factory.Expr.Equal("p.user_id", "u.id"));

			Assert.Equal("SELECT u.*, p.* FROM users u INNER JOIN phones p ON p.user_id = u.id", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithRightJoin()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.RightJoin("u", "phones", "p", _factory.Expr.Equal("p.user_id", "u.id"));

			Assert.Equal("SELECT u.*, p.* FROM users u RIGHT JOIN phones p ON p.user_id = u.id", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithAndWhereConditions()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.Where("u.username = @Username")
				.AndWhere("u.name = @Name");
			
			Assert.Equal("SELECT u.*, p.* FROM users u WHERE (u.username = @Username) AND (u.name = @Name)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithOrWhereConditions()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.Where("u.username = @Username")
				.OrWhere("u.name = @Name");

			Assert.Equal("SELECT u.*, p.* FROM users u WHERE (u.username = @Username) OR (u.name = @Name)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithOrOrWhereConditions()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.OrWhere("u.username = @Username")
				.OrWhere("u.name = @Name");

			Assert.Equal("SELECT u.*, p.* FROM users u WHERE (u.username = @Username) OR (u.name = @Name)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectWithAndOrWhereConditions()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.Where("u.username = @Username1")
				.AndWhere("u.username = @Username2")
				.OrWhere("u.name = @Name1")
				.AndWhere("u.name = @Name2");

			Assert.Equal("SELECT u.*, p.* FROM users u WHERE (((u.username = @Username1) AND (u.username = @Username2)) OR (u.name = @Name1)) AND (u.name = @Name2)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectGroupBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectEmptyGroupBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy();

			Assert.Equal("SELECT u.*, p.* FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectEmptyAddGroupBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.AddGroupBy();

			Assert.Equal("SELECT u.*, p.* FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAddGroupBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.AddGroupBy("u.foo");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id, u.foo", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAddGroupByMultiple()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.AddGroupBy("u.foo", "u.bar");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id, u.foo, u.bar", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.Having("u.name = @Name");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING u.name = @Name", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAndHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.AndHaving("u.name = @Name");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING u.name = @Name", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectHavingAndHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.Having("u.name = @Name")
				.AndHaving("u.username = @Username");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING (u.name = @Name) AND (u.username = @Username)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectHavingOrHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.Having("u.name = @Name")
				.OrHaving("u.username = @Username");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING (u.name = @Name) OR (u.username = @Username)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectOrHavingOrHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.OrHaving("u.name = @Name")
				.OrHaving("u.username = @Username");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING (u.name = @Name) OR (u.username = @Username)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectHavingAndOrHaving()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.GroupBy("u.id")
				.Having("u.name = @Name")
				.OrHaving("u.username = @Username1")
				.AndHaving("u.username = @Username2");

			Assert.Equal("SELECT u.*, p.* FROM users u GROUP BY u.id HAVING ((u.name = @Name) OR (u.username = @Username1)) AND (u.username = @Username2)", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectOrderBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.OrderBy("u.name");

			Assert.Equal("SELECT u.*, p.* FROM users u ORDER BY u.name ASC", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAddOrderBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.OrderBy("u.name")
				.AddOrderBy("u.username", "DESC");

			Assert.Equal("SELECT u.*, p.* FROM users u ORDER BY u.name ASC, u.username DESC", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAddAddOrderBy()
		{
			var qb = _factory
				.Select("u.*", "p.*")
				.From("users", "u")
				.AddOrderBy("u.name")
				.AddOrderBy("u.username", "DESC");

			Assert.Equal("SELECT u.*, p.* FROM users u ORDER BY u.name ASC, u.username DESC", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectAddColumns()
		{
			var qb = _factory
				.Select("u.*")
				.AddColumns("p.*")
				.From("users", "u");

			Assert.Equal("SELECT u.*, p.* FROM users u", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectMultipleFrom()
		{
			var qb = _factory
				.Select("u.*")
				.AddColumns("p.*")
				.From("users", "u")
				.From("phonenumbers", "p");

			Assert.Equal("SELECT u.*, p.* FROM users u, phonenumbers p", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectLimit()
		{
			var qb = _factory
				.Select("u.*")
				.From("users", "u")
				.SetLimit(10);

			Assert.Equal("SELECT u.* FROM users u LIMIT 10", qb.ToSql());
		}

		[Fact]
		public void ToSql_SelectLimitOffset()
		{
			var qb = _factory
				.Select("u.*")
				.From("users", "u")
				.SetLimit(10)
				.SetOffset(5);

			Assert.Equal("SELECT u.* FROM users u LIMIT 10 OFFSET 5", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectFromMasterWithWhereOnJoinedTables()
		{
			var qb = _factory
				.Select("COUNT(DISTINCT news.id)")
				.From("newspages", "news")
				.InnerJoin("news", "nodeversion", "nv", "nv.refId = news.id AND nv.refEntityname='Entity\\News'")
				.InnerJoin("nv", "nodetranslation", "nt", "nv.nodetranslation = nt.id")
				.InnerJoin("nt", "node", "n", "nt.node = n.id")
				.Where("nt.lang = @Lang")
				.AndWhere("n.deleted = 0");
			
			Assert.Equal("SELECT COUNT(DISTINCT news.id) FROM newspages news INNER JOIN nodeversion nv ON nv.refId = news.id AND nv.refEntityname='Entity\\News' INNER JOIN nodetranslation nt ON nv.nodetranslation = nt.id INNER JOIN node n ON nt.node = n.id WHERE (nt.lang = @Lang) AND (n.deleted = 0)", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectWithMultipleFromAndJoins()
		{
			var qb = _factory
				.Select("DISTINCT u.id")
				.From("users", "u")
				.From("articles", "a")
				.InnerJoin("u", "permissions", "p", "p.user_id = u.id")
				.InnerJoin("a", "comments", "c", "c.article_id = a.id")
				.Where("u.id = a.user_id")
				.AndWhere("p.read = 1");
			
			Assert.Equal("SELECT DISTINCT u.id FROM users u INNER JOIN permissions p ON p.user_id = u.id, articles a INNER JOIN comments c ON c.article_id = a.id WHERE (u.id = a.user_id) AND (p.read = 1)", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectWithJoinsWithMultipleOnConditionsParseOrder()
		{
			var qb = _factory
				.Select("a.id")
				.From("table_a", "a")
				.Join("a", "table_b", "b", "a.fk_b = b.id")
				.Join("b", "table_c", "c", "c.fk_b = b.id AND b.language = @Language")
				.Join("a", "table_d", "d", "a.fk_d = d.id")
				.Join("c", "table_e", "e", "e.fk_c = c.id AND e.fk_d = d.id");
			
			Assert.Equal(
				"SELECT a.id " +
				"FROM table_a a " +
				"INNER JOIN table_b b ON a.fk_b = b.id " +
				"INNER JOIN table_d d ON a.fk_d = d.id " +
				"INNER JOIN table_c c ON c.fk_b = b.id AND b.language = @Language " +
				"INNER JOIN table_e e ON e.fk_c = c.id AND e.fk_d = d.id",
				qb.ToSql()
			);
		}

		[Fact]
		public void ToString_SelectWithMultipleFromsAndJoinsWithMultipleOnConditionsParseOrder()
		{
			var qb = _factory
				.Select("a.id")
				.From("table_a", "a")
				.From("table_f", "f")
				.Join("a", "table_b", "b", "a.fk_b = b.id")
				.Join("b", "table_c", "c", "c.fk_b = b.id AND b.language = @Language")
				.Join("a", "table_d", "d", "a.fk_d = d.id")
				.Join("c", "table_e", "e", "e.fk_c = c.id AND e.fk_d = d.id")
				.Join("f", "table_g", "g", "f.fk_g = g.id");
			
			Assert.Equal(
				"SELECT a.id " +
				"FROM table_a a " +
				"INNER JOIN table_b b ON a.fk_b = b.id " +
				"INNER JOIN table_d d ON a.fk_d = d.id " +
				"INNER JOIN table_c c ON c.fk_b = b.id AND b.language = @Language " +
				"INNER JOIN table_e e ON e.fk_c = c.id AND e.fk_d = d.id, " +
				"table_f f " +
				"INNER JOIN table_g g ON f.fk_g = g.id",
				qb.ToSql()
			);
		}

		[Fact]
		public void ToString_SimpleSelectWithoutTableAlias()
		{
			var qb = _factory
				.Select("id")
				.From("users");
			
			Assert.Equal("SELECT id FROM users", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectWithSimpleWhereWithoutTableAlias()
		{
			var qb = _factory
				.Select("id", "name")
				.From("users")
				.Where("awesome=9001");

			Assert.Equal("SELECT id, name FROM users WHERE awesome=9001", qb.ToSql());
		}

		[Fact]
		public void ToString_ComplexSelectWithoutTableAliases()
		{
			var qb = _factory
				.Select("DISTINCT users.id")
				.From("users")
				.From("articles")
				.Join("users", "permissions", "p", "p.user_id = users.id")
				.Join("articles", "comments", "c", "c.article_id = articles.id")
				.Where("users.id = articles.user_id")
				.AndWhere("p.read = 1");

			Assert.Equal("SELECT DISTINCT users.id FROM users INNER JOIN permissions p ON p.user_id = users.id, articles INNER JOIN comments c ON c.article_id = articles.id WHERE (users.id = articles.user_id) AND (p.read = 1)", qb.ToSql());
		}

		[Fact]
		public void ToString_ComplexSelectWithSomeTableAliases()
		{
			var qb = _factory
				.Select("u.id")
				.From("users", "u")
				.From("articles")
				.InnerJoin("u", "permissions", "p", "p.user_id = u.id")
				.InnerJoin("articles", "comments", "c", "c.article_id = articles.id");

			Assert.Equal("SELECT u.id FROM users u INNER JOIN permissions p ON p.user_id = u.id, articles INNER JOIN comments c ON c.article_id = articles.id", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectAllFromTableWithoutTableAlias()
		{
			var qb = _factory
				.Select("users.*")
				.From("users");

			Assert.Equal("SELECT users.* FROM users", qb.ToSql());
		}

		[Fact]
		public void ToString_SelectAllWithoutTableAlias()
		{
			var qb = _factory
				.Select("*")
				.From("users");

			Assert.Equal("SELECT * FROM users", qb.ToSql());
		}

		[Fact]
		public void ToSql_AddWhereShouldNotMutate()
		{
			var qb = _factory
				.Select("*")
				.From("users")
				.Where("active = TRUE");

			qb.AndWhere("disabled = FALSE");

			Assert.Equal("SELECT * FROM users WHERE active = TRUE", qb.ToSql());
		}

		[Fact]
		public void ToSql_ClearFrom()
		{
			var qb = _factory
				.Select("some_function()")
				.From("users")
				.ClearFrom();

			Assert.Equal("SELECT some_function()", qb.ToSql());
		}
	}
}
