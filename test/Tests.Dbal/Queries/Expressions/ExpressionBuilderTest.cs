using System.Collections.Generic;
using DKX.Dbal.Queries.Expressions;
using Xunit;

namespace DKX.DbalTests.Queries.Expressions
{
	public class ExpressionBuilderTest
	{
		private readonly ExpressionBuilder _expr = new ExpressionBuilder();

		[Theory]
		[MemberData(nameof(DataForAnd))]
		public void And(string[] parts, string expected)
		{
			Assert.Equal(expected, _expr.And(parts));
		}

		public static IEnumerable<object[]> DataForAnd()
		{
			return new []
			{
				new object[]
				{
					new [] {"u.user = 1"},
					"u.user = 1",
				},
				new object[]
				{
					new [] {"u.user = 1", "u.group_id = 1"},
					"(u.user = 1) AND (u.group_id = 1)",
				},
				new object[]
				{
					new [] {"u.group_id = 1", "u.group_id = 2"},
					"(u.group_id = 1) AND (u.group_id = 2)",
				},
			};
		}

		[Theory]
		[MemberData(nameof(DataForOr))]
		public void Or(string[] parts, string expected)
		{
			Assert.Equal(expected, _expr.Or(parts));
		}

		public static IEnumerable<object[]> DataForOr()
		{
			return new []
			{
				new object[]
				{
					new [] {"u.user = 1"},
					"u.user = 1",
				},
				new object[]
				{
					new [] {"u.user = 1", "u.group_id = 1"},
					"(u.user = 1) OR (u.group_id = 1)",
				},
				new object[]
				{
					new [] {"u.group_id = 1", "u.group_id = 2"},
					"(u.group_id = 1) OR (u.group_id = 2)",
				},
			};
		}

		[Theory]
		[MemberData(nameof(DataForComparison))]
		public void Comparison(string leftExpr, string op, string rightExpr, string expected)
		{
			Assert.Equal(expected, _expr.Comparison(leftExpr, op, rightExpr));
		}

		public static IEnumerable<object[]> DataForComparison()
		{
			return new[]
			{
				new object[] {"u.user_id", "=", "1", "u.user_id = 1"},
				new object[] {"u.user_id", "<>", "1", "u.user_id <> 1"},
				new object[] {"u.salary", "<", "10000", "u.salary < 10000"},
				new object[] {"u.salary", "<=", "10000", "u.salary <= 10000"},
				new object[] {"u.salary", ">", "10000", "u.salary > 10000"},
				new object[] {"u.salary", ">=", "10000", "u.salary >= 10000"},
			};
		}

		[Fact]
		public void Equal()
		{
			Assert.Equal("u.user_id = 1", _expr.Equal("u.user_id", "1"));
		}

		[Fact]
		public void NotEqual()
		{
			Assert.Equal("u.user_id <> 1", _expr.NotEqual("u.user_id", "1"));
		}

		[Fact]
		public void LowerThan()
		{
			Assert.Equal("u.salary < 10000", _expr.LowerThan("u.salary", "10000"));
		}

		[Fact]
		public void LowerThanOrEqual()
		{
			Assert.Equal("u.salary <= 10000", _expr.LowerThanOrEqual("u.salary", "10000"));
		}

		[Fact]
		public void GreaterThan()
		{
			Assert.Equal("u.salary > 10000", _expr.GreaterThan("u.salary", "10000"));
		}

		[Fact]
		public void GreaterThanOrEqual()
		{
			Assert.Equal("u.salary >= 10000", _expr.GreaterThanOrEqual("u.salary", "10000"));
		}

		[Fact]
		public void IsNull()
		{
			Assert.Equal("u.deleted IS NULL", _expr.IsNull("u.deleted"));
		}

		[Fact]
		public void IsNotNull()
		{
			Assert.Equal("u.deleted IS NOT NULL", _expr.IsNotNull("u.deleted"));
		}

		[Fact]
		public void In()
		{
			Assert.Equal("u.groups IN (1, 3, 4, 7)", _expr.In("u.groups", new [] {"1", "3", "4", "7"}));
		}

		[Fact]
		public void InWithPlaceholder()
		{
			Assert.Equal("u.groups IN (@Group)", _expr.In("u.groups", "@Group"));
		}

		[Fact]
		public void NotIn()
		{
			Assert.Equal("u.groups NOT IN (1, 3, 4, 7)", _expr.NotIn("u.groups", new [] {"1", "3", "4", "7"}));
		}

		[Fact]
		public void NotInWithPlaceholder()
		{
			Assert.Equal("u.groups NOT IN (@Group)", _expr.NotIn("u.groups", "@Group"));
		}

		[Fact]
		public void LikeWithoutEscape()
		{
			Assert.Equal("a.song LIKE 'a virgin'", _expr.Like("a.song", "'a virgin'"));
		}

		[Fact]
		public void LikeWithEscape()
		{
			Assert.Equal("a.song LIKE 'a virgin' ESCAPE '💩'", _expr.Like("a.song", "'a virgin'", "'💩'"));
		}

		[Fact]
		public void NotLikeWithoutEscape()
		{
			Assert.Equal("s.last_words NOT LIKE 'this'", _expr.NotLike("s.last_words", "'this'"));
		}

		[Fact]
		public void NotLikeWithEscape()
		{
			Assert.Equal("p.description NOT LIKE '20💩%' ESCAPE '💩'", _expr.NotLike("p.description", "'20💩%'", "'💩'"));
		}
	}
}
