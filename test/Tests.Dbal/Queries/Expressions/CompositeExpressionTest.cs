using System.Collections.Generic;
using System.Collections.Immutable;
using DKX.Dbal.Queries.Expressions;
using Xunit;

namespace DKX.DbalTests.Queries.Expressions
{
	public class CompositeExpressionTest
	{
		[Fact]
		public void Length()
		{
			var expr = new CompositeExpression(CompositeExpressionType.Or, new StringExpression("u.group_id = 1"));

			Assert.Equal(1, expr.Length);

			expr = expr.Add(new CompositeExpression(CompositeExpressionType.And));

			Assert.Equal(2, expr.Length);

			expr = expr.Add(new StringExpression("u.group_id = 2"));

			Assert.Equal(3, expr.Length);
		}

		[Theory]
		[MemberData(nameof(GetToSqlData))]
		public void ToSql(CompositeExpressionType type, IImmutableList<IExpression> parts, string expects)
		{
			var expr = new CompositeExpression(type, parts);

			Assert.Equal(expects, expr.ToSql());
		}

		[Fact]
		public void Merge_IntoExistingSame()
		{
			var into = new CompositeExpression(CompositeExpressionType.Or, ImmutableList.Create<IExpression>(new StringExpression("u.group_id = 1"), new StringExpression("u.group_id = 2")));
			var expr = new StringExpression("u.group_id = 3");
			var result = CompositeExpression.Merge(CompositeExpressionType.Or, expr, into);

			Assert.Equal("(u.group_id = 1) OR (u.group_id = 2) OR (u.group_id = 3)", result.ToSql());
		}
		
		[Fact]
		public void Merge_IntoExistingDifferent()
		{
			var into = new CompositeExpression(CompositeExpressionType.And, ImmutableList.Create<IExpression>(new StringExpression("u.group_id = 1"), new StringExpression("u.group_id = 2")));
			var expr = new StringExpression("u.group_id = 3");
			var result = CompositeExpression.Merge(CompositeExpressionType.Or, expr, into);

			Assert.Equal("((u.group_id = 1) AND (u.group_id = 2)) OR (u.group_id = 3)", result.ToSql());
		}

		[Fact]
		public void Merge_IntoNew()
		{
			var expr = new StringExpression("u.group_id = 1");
			var result = CompositeExpression.Merge(CompositeExpressionType.Or, expr);

			Assert.Equal("u.group_id = 1", result.ToSql());
		}
		
		[Fact]
		public void Add_ShouldNotMutate()
		{
			var expr = new CompositeExpression(CompositeExpressionType.Or, new StringExpression("u.group_id = 1"));

			Assert.Equal(1, expr.Length);
			Assert.Equal("u.group_id = 1", expr.ToSql());

			expr.Add(new CompositeExpression(CompositeExpressionType.And));

			Assert.Equal(1, expr.Length);
			Assert.Equal("u.group_id = 1", expr.ToSql());
		}
		
		[Fact]
		public void Merge_ShouldNotMutate()
		{
			var into = new CompositeExpression(CompositeExpressionType.Or, new StringExpression("u.group_id = 1"));

			Assert.Equal(1, into.Length);
			Assert.Equal("u.group_id = 1", into.ToSql());
			
			CompositeExpression.Merge(CompositeExpressionType.Or, new StringExpression("u.group_id = 3"), into);

			Assert.Equal(1, into.Length);
			Assert.Equal("u.group_id = 1", into.ToSql());
		}

		public static IEnumerable<object[]> GetToSqlData()
		{
			return new[]
			{
				new object[]
				{
					CompositeExpressionType.And,
					ImmutableList.Create<IExpression>(new StringExpression("u.user = 1")),
					"u.user = 1",
				},
				new object[]
				{
					CompositeExpressionType.And,
					ImmutableList.Create<IExpression>(new StringExpression("u.user = 1"), new StringExpression("u.group_id = 1")),
					"(u.user = 1) AND (u.group_id = 1)",
				},
				new object[]
				{
					CompositeExpressionType.Or,
					ImmutableList.Create<IExpression>(new StringExpression("u.user = 1")),
					"u.user = 1",
				},
				new object[]
				{
					CompositeExpressionType.Or,
					ImmutableList.Create<IExpression>(new StringExpression("u.group_id = 1"), new StringExpression("u.group_id = 2")),
					"(u.group_id = 1) OR (u.group_id = 2)",
				},
				new object[]
				{
					CompositeExpressionType.And,
					ImmutableList.Create<IExpression>(
						new StringExpression("u.user = 1"), 
						new CompositeExpression(
							CompositeExpressionType.Or,
							ImmutableList.Create<IExpression>(new StringExpression("u.group_id = 1"), new StringExpression("u.group_id = 2"))
						)
					),
					"(u.user = 1) AND ((u.group_id = 1) OR (u.group_id = 2))",
				},
				new object[]
				{
					CompositeExpressionType.Or,
					ImmutableList.Create<IExpression>(
						new StringExpression("u.group_id = 1"), 
						new CompositeExpression(
							CompositeExpressionType.And,
							ImmutableList.Create<IExpression>(new StringExpression("u.user = 1"), new StringExpression("u.group_id = 2"))
						)
					),
					"(u.group_id = 1) OR ((u.user = 1) AND (u.group_id = 2))",
				},
			};
		}
	}
}
