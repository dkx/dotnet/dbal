using DKX.Dbal.Queries.Expressions;
using Xunit;

namespace DKX.DbalTests.Queries.Expressions
{
	public class StringExpressionTest
	{
		[Fact]
		public void ToSql()
		{
			var expr = new StringExpression("u.group_id = 1");

			Assert.Equal("u.group_id = 1", expr.ToSql());
		}
	}
}
