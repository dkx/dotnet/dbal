using System.Data.Common;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;
using Moq;
using Xunit;

namespace DKX.DbalTests.Queries
{
	public class UpdateQueryBuilderTest
	{
		private readonly IQueryBuilderFactory _factory;

		public UpdateQueryBuilderTest()
		{
			var driver = new AdoDriver(new Mock<DbConnection>().Object);
			var connection = new Mock<IConnection>();
			connection.Setup(c => c.ToDriver()).Returns(driver);

			_factory = new QueryBuilderFactory(connection.Object, new ActivatorRowMapper());
		}

		[Fact]
		public void ToString_Update()
		{
			var qb = _factory
				.Update("users", "u")
				.Set("u.foo", "@Foo")
				.Set("u.bar", "@Bar");

			Assert.Equal("UPDATE users u SET u.bar = @Bar, u.foo = @Foo", qb.ToSql());
		}

		[Fact]
		public void ToString_UpdateWithoutAlias()
		{
			var qb = _factory
				.Update("users")
				.Set("foo", "@Foo")
				.Set("bar", "@Bar");

			Assert.Equal("UPDATE users SET bar = @Bar, foo = @Foo", qb.ToSql());
		}

		[Fact]
		public void ToString_UpdateWhere()
		{
			var qb = _factory
				.Update("users", "u")
				.Set("u.foo", "@Foo")
				.Where("u.foo = @Foo");

			Assert.Equal("UPDATE users u SET u.foo = @Foo WHERE u.foo = @Foo", qb.ToSql());
		}

		[Fact]
		public void ToString_UpdateWithInit()
		{
			var qb = _factory
				.Update("users", null, new
				{
					foo = (string?) null,
					bar = 10,
				});

			Assert.Equal("UPDATE users SET bar = @UpdateInit1, foo = @UpdateInit0", qb.ToSql());
		}

		[Fact]
		public void IsValid_False()
		{
			var qb = _factory.Update("users");

			Assert.False(qb.IsValid());
		}

		[Fact]
		public void IsValid_True()
		{
			var qb = _factory
				.Update("users")
				.Set("email", "NULL");

			Assert.True(qb.IsValid());
		}
	}
}
