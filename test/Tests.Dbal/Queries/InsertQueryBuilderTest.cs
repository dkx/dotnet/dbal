using System.Collections.Generic;
using System.Data.Common;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;
using Moq;
using Xunit;

namespace DKX.DbalTests.Queries
{
	public class InsertQueryBuilderTest
	{
		private readonly IQueryBuilderFactory _factory;

		public InsertQueryBuilderTest()
		{
			var driver = new AdoDriver(new Mock<DbConnection>().Object);
			var connection = new Mock<IConnection>();
			connection.Setup(c => c.ToDriver()).Returns(driver);

			_factory = new QueryBuilderFactory(connection.Object, new ActivatorRowMapper());
		}

		[Fact]
		public void ToString_InsertValues()
		{
			var qb = _factory
				.Insert("users")
				.Values(new Dictionary<string, string>
				{
					["foo"] = "@Foo",
					["bar"] = "@Bar",
				});

			Assert.Equal("INSERT INTO users (bar, foo) VALUES (@Bar, @Foo)", qb.ToSql());
		}

		[Fact]
		public void ToString_InsertReplaceValues()
		{
			var qb = _factory
				.Insert("users")
				.Values(new Dictionary<string, string>
				{
					["foo"] = "@Foo",
					["bar"] = "@Bar",
				})
				.Values(new Dictionary<string, string>
				{
					["bar"] = "@Bar",
					["foo"] = "@Foo",
				});

			Assert.Equal("INSERT INTO users (bar, foo) VALUES (@Bar, @Foo)", qb.ToSql());
		}

		[Fact]
		public void ToString_InsertSetValue()
		{
			var qb = _factory
				.Insert("users")
				.SetValue("foo", "bar")
				.SetValue("bar", "@Bar")
				.SetValue("foo", "@Foo");

			Assert.Equal("INSERT INTO users (bar, foo) VALUES (@Bar, @Foo)", qb.ToSql());
		}

		[Fact]
		public void ToString_InsertValuesSetValue()
		{
			var qb = _factory
				.Insert("users")
				.Values(new Dictionary<string, string>
				{
					["foo"] = "@Foo",
				})
				.SetValue("bar", "@Bar");

			Assert.Equal("INSERT INTO users (bar, foo) VALUES (@Bar, @Foo)", qb.ToSql());
		}

		[Fact]
		public void ToString_InsertInit()
		{
			var qb = _factory
				.Insert("users", new
				{
					foo = (string?) null,
					bar = 42,
				});

			Assert.Equal("INSERT INTO users (bar, foo) VALUES (@InsertInit1, @InsertInit0)", qb.ToSql());
		}
	}
}
