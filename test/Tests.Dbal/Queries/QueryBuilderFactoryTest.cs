using System.Data.Common;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Queries;
using DKX.Dbal.RowMapping;
using Moq;
using Xunit;

namespace DKX.DbalTests.Queries
{
	public class QueryBuilderFactoryTest
	{
		private readonly QueryBuilderFactory _factory;

		public QueryBuilderFactoryTest()
		{
			var driver = new AdoDriver(new Mock<DbConnection>().Object);
			var connection = new Mock<IConnection>();
			connection.Setup(c => c.ToDriver()).Returns(driver);

			_factory = new QueryBuilderFactory(connection.Object, new ActivatorRowMapper());
		}

		[Fact]
		public void Select()
		{
			var qb = _factory.Select("*");

			Assert.IsAssignableFrom<ISelectQueryBuilder>(qb);
		}

		[Fact]
		public void Update()
		{
			var qb = _factory.Update("book");

			Assert.IsAssignableFrom<IUpdateQueryBuilder>(qb);
		}

		[Fact]
		public void Delete()
		{
			var qb = _factory.Delete("book");

			Assert.IsAssignableFrom<IDeleteQueryBuilder>(qb);
		}

		[Fact]
		public void Insert()
		{
			var qb = _factory.Insert("book");

			Assert.IsAssignableFrom<IInsertQueryBuilder>(qb);
		}
	}
}
