using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal;
using Moq;
using Xunit;

namespace DKX.DbalTests
{
	public class MiddlewareStackTest
	{
		private readonly MiddlewareStack _middlewares = new MiddlewareStack();
		
		[Fact]
		public async Task Run()
		{
			var logs = new List<string>();

			_middlewares.Add(new MiddlewareA(logs));
			_middlewares.Add(new MiddlewareB(logs));
			_middlewares.Add(new MiddlewareC(logs));
			
			var query = new Mock<IQuery>();
			var result = await _middlewares.Run(query.Object, () =>
			{
				logs.Add("Result");
				return Task.FromResult(42);
			}, CancellationToken.None);

			Assert.Equal(42, result);
			Assert.Equal(new[]
			{
				"A:Before",
				"B:Before",
				"C:Before",
				"Result",
				"C:After",
				"B:After",
				"A:After",
			}, logs.ToArray());
		}

		private class MiddlewareA : IMiddleware
		{
			private readonly IList<string> _logs;

			public MiddlewareA(IList<string> logs)
			{
				_logs = logs;
			}
			
			public async Task<TResult> Handle<TResult>(IQuery query, Next<TResult> next, CancellationToken cancellationToken)
			{
				_logs.Add("A:Before");
				var result = await next();
				Assert.Equal(42, (object) result!);
				_logs.Add("A:After");
				return result;
			}
		}

		private class MiddlewareB : IMiddleware
		{
			private readonly IList<string> _logs;

			public MiddlewareB(IList<string> logs)
			{
				_logs = logs;
			}
			
			public async Task<TResult> Handle<TResult>(IQuery query, Next<TResult> next, CancellationToken cancellationToken)
			{
				_logs.Add("B:Before");
				var result = await next();
				Assert.Equal(42, (object) result!);
				_logs.Add("B:After");
				return result;
			}
		}

		private class MiddlewareC : IMiddleware
		{
			private readonly IList<string> _logs;

			public MiddlewareC(IList<string> logs)
			{
				_logs = logs;
			}
			
			public async Task<TResult> Handle<TResult>(IQuery query, Next<TResult> next, CancellationToken cancellationToken)
			{
				_logs.Add("C:Before");
				var result = await next();
				Assert.Equal(42, (object) result!);
				_logs.Add("C:After");
				return result;
			}
		}
	}
}
