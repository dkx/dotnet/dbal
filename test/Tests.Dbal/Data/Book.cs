using System;

namespace DKX.DbalTests.Data
{
	public class Book
	{
		public Book(long id, long? editorId)
		{
			Id = id;
			EditorId = editorId;
		}
		
		public long Id { get; }
		
		public long? EditorId { get; }
	}
}
