namespace DKX.DbalTests.Data
{
	public class User
	{
		public User(long id, string email)
		{
			Id = id;
			Email = email;
		}
			
		public long Id { get; }
			
		public string Email { get; }
	}
}
