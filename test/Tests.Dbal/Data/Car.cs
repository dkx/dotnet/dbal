namespace DKX.DbalTests.Data
{
	public class Car
	{
		public Car(int id, Color color)
		{
			Id = id;
			Color = color;
		}
		
		public int Id { get; }
		
		public Color Color { get; }
	}
}