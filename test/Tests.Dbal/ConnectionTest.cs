using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using Moq;
using Moq.Protected;
using Xunit;

namespace DKX.DbalTests
{
	public class ConnectionTest
	{
		[Fact]
		public async Task Disposing()
		{
			var dbConnection = new Mock<DbConnection>();
			var driver = new Mock<IDriver>();
			driver.Setup(m => m.ToDbConnection()).Returns(dbConnection.Object);
			
			var connection = new Connection(driver.Object);
			
			await connection.DisposeAsync();

			dbConnection.Verify(m => m.DisposeAsync(), Times.Once);
		}

		[Fact]
		public void Connect()
		{
			var dbConnection = new Mock<DbConnection>();
			dbConnection
				.SetupSequence(m => m.State)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Open)
				.Returns(ConnectionState.Open);
			
			var driver = new Mock<IDriver>();
			driver.Setup(m => m.ToDbConnection()).Returns(dbConnection.Object);
			
			var connection = new Connection(driver.Object);

			connection.Connect();
			connection.Connect();
			connection.Connect();

			dbConnection.Verify(m => m.OpenAsync(It.IsAny<CancellationToken>()), Times.Once);
		}

		[Fact]
		public void Disconnect()
		{
			var dbConnection = new Mock<DbConnection>();
			dbConnection
				.SetupSequence(m => m.State)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Open)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Closed);

			var driver = new Mock<IDriver>();
			driver.Setup(m => m.ToDbConnection()).Returns(dbConnection.Object);
			
			var connection = new Connection(driver.Object);

			connection.Disconnect();
			connection.Disconnect();
			connection.Disconnect();
			connection.Connect();
			connection.Disconnect();
			connection.Disconnect();
			connection.Disconnect();

			dbConnection.Verify(m => m.OpenAsync(It.IsAny<CancellationToken>()), Times.Once);
			dbConnection.Verify(m => m.CloseAsync(), Times.Once);
		}

		[Fact]
		public void Reconnect()
		{
			var dbConnection = new Mock<DbConnection>();
			dbConnection
				.SetupSequence(m => m.State)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Open)
				.Returns(ConnectionState.Closed)
				.Returns(ConnectionState.Open)
				.Returns(ConnectionState.Closed);
			
			var driver = new Mock<IDriver>();
			driver.Setup(m => m.ToDbConnection()).Returns(dbConnection.Object);
			
			var connection = new Connection(driver.Object);

			connection.Reconnect();
			connection.Reconnect();
			connection.Reconnect();

			dbConnection.Verify(m => m.OpenAsync(It.IsAny<CancellationToken>()), Times.Exactly(3));
			dbConnection.Verify(m => m.CloseAsync(), Times.Exactly(2));
		}

		[Fact]
		public async Task Execute()
		{
			var dbConnection = new Mock<DbConnection>();
			dbConnection.Protected().Setup<DbCommand>("CreateDbCommand").Returns(() =>
			{
				var command = new Mock<DbCommand>();
				command.Setup(mc => mc.ExecuteNonQueryAsync(It.IsAny<CancellationToken>())).Returns(Task.FromResult(42));

				return command.Object;
			});
			
			var driver = new Mock<IDriver>();
			driver.Setup(m => m.ToDbConnection()).Returns(dbConnection.Object);
			
			var connection = new Connection(driver.Object);
			var result = await connection.Execute("SELECT 1");

			Assert.Equal(42, result);

			dbConnection.Verify(m => m.OpenAsync(It.IsAny<CancellationToken>()), Times.Once);
		}
	}
}
