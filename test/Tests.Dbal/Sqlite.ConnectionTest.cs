using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.DbalTests.Data;
using Microsoft.Data.Sqlite;
using Xunit;

namespace DKX.DbalTests
{
	public class SqliteConnectionTest
	{
		[Fact]
		public async Task BeginTransaction_Savepoints_NotSupported()
		{
			await using var connection = CreateConnection();
			await using (var transaction01 = await connection.BeginTransaction())
			{
				Assert.IsAssignableFrom<ITransaction>(transaction01);
				var e = await Assert.ThrowsAsync<NotSupportedException>(() => connection.BeginTransaction());
				Assert.Equal("DKX.Dbal.Drivers.AdoDriver does not support nested transactions (savepoints)", e.Message);
			}
		}
		
		[Fact]
		public async Task Execute()
		{
			await using var connection = CreateConnection();
			var rows = await connection.Execute("SELECT 1");
			
			Assert.Equal(-1, rows);
		}

		[Fact]
		public async Task Execute_WithParameters()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			
			var rows = await connection.Execute("INSERT INTO users (email) VALUES (@Email)", new
			{
				Email = "john@doe.com",
			});

			Assert.Equal(1, rows);
			
			rows = await connection.Execute("INSERT INTO users (email) VALUES (@Email)", new
			{
				Email = "lord@voldemort.com",
			});

			Assert.Equal(1, rows);

			rows = await connection.Execute("UPDATE users SET email = @NewEmail", new
			{
				NewEmail = "hello@world.com",
			});

			Assert.Equal(2, rows);
		}

		[Fact]
		public async Task ExecuteScalar()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

			var email = await connection.ExecuteScalar<string>("SELECT email FROM users LIMIT 1");

			Assert.Equal("john@doe.com", email);
		}

		[Fact]
		public async Task ExecuteScalar_WithParameters()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT NOT NULL, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (name, email) VALUES ('John Doe', 'john@doe.com')");

			var email = await connection.ExecuteScalar<string>("SELECT email FROM users WHERE name = @UserName LIMIT 1", new
			{
				UserName = "John Doe",
			});

			Assert.Equal("john@doe.com", email);
		}

		[Fact]
		public async Task ExecuteScalar_Nullable()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var email = await connection.ExecuteScalar<string?>("SELECT email FROM users LIMIT 1");

			Assert.Null(email);
		}

		[Fact]
		public async Task ExecuteScalar_NullableColumn()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, name TEXT DEFAULT NULL)");
			await connection.Execute("INSERT INTO users (name) VALUES (NULL)");

			var email = await connection.ExecuteScalar<string?>("SELECT name FROM users LIMIT 1");

			Assert.Null(email);
		}

		[Fact]
		public async Task Query()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('lord@voldemort.com')");

			await foreach (var user in connection.Query<User>("SELECT id, email FROM users ORDER BY id"))
			{
				// test running another inside of await foreach
				await connection.Execute("SELECT 1");
				
				Assert.IsType<User>(user);

				switch (user.Id)
				{
					case 1: Assert.Equal("john@doe.com", user.Email); break;
					case 2: Assert.Equal("lord@voldemort.com", user.Email); break;
					default: throw new ArgumentOutOfRangeException();
				}
			}
		}

		[Fact]
		public async Task Query_Anonymous()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (id, email) VALUES (42, 'john@doe.com')");

			var user = (await connection.Query("SELECT id, email FROM users ORDER BY id").ToArrayAsync()).First();

			Assert.Equal(new Dictionary<string, object?>
			{
				["id"] = 42L,
				["email"] = "john@doe.com",
			}, user);
		}

		[Fact]
		public async Task QueryScalar_Throws_MultipleColumns()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('lord@voldemort.com')");

			var e = await Assert.ThrowsAsync<DbalException>(async () => await connection.QueryScalar<string>("SELECT id, email FROM users ORDER BY id").ToArrayAsync());
			Assert.Equal("ToScalarResult: query 'SELECT id, email FROM users ORDER BY id' returns more than one columns", e.Message);
		}

		[Fact]
		public async Task QueryScalar()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('lord@voldemort.com')");

			var emails = await connection.QueryScalar<string>("SELECT email FROM users ORDER BY id").ToArrayAsync();

			Assert.Equal(new []
			{
				"john@doe.com",
				"lord@voldemort.com",
			}, emails);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Throws_ManyRows()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('lord@voldemort.com')");

			var e = await Assert.ThrowsAsync<DbalException>(() => connection.QuerySingleOrDefault<User>("SELECT id, email FROM users"));
			Assert.Equal("Query must return only one row (SELECT id, email FROM users)", e.Message);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Throws_NoSuitableConstructor()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

			var e = await Assert.ThrowsAsync<DbalException>(() => connection.QuerySingleOrDefault<UserWrongConstructor>("SELECT id, email FROM users"));
			Assert.Equal("DKX.DbalTests.Data.UserWrongConstructor does not have any suitable constructor for [System.Int64, System.String]", e.Message);
		}

		[Fact]
		public async Task QuerySingleOrDefault()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

			var user = await connection.QuerySingleOrDefault<User>("SELECT id, email FROM users");

			Assert.NotNull(user);
			Assert.IsType<User>(user);
			Assert.Equal(1, user!.Id);
			Assert.Equal("john@doe.com", user!.Email);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Returns_Null()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var user = await connection.QuerySingleOrDefault<User>("SELECT id, email FROM users");

			Assert.Null(user);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Anonymous_Throws_ManyRows()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('lord@voldemort.com')");

			var e = await Assert.ThrowsAsync<DbalException>(() => connection.QuerySingleOrDefault("SELECT id, email FROM users"));
			Assert.Equal("Query must return only one row (SELECT id, email FROM users)", e.Message);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Anonymous()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (id, email) VALUES (42, 'john@doe.com')");

			var user = await connection.QuerySingleOrDefault("SELECT id, email FROM users");

			Assert.NotNull(user);
			Assert.Equal(new Dictionary<string, object?>
			{
				["id"] = 42L,
				["email"] = "john@doe.com",
			}, user);
		}

		[Fact]
		public async Task QuerySingleOrDefault_Anonymous_Null()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var user = await connection.QuerySingleOrDefault("SELECT id, email FROM users");

			Assert.Null(user);
		}

		[Fact]
		public async Task QuerySingle_Throws_ReturnsNull()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var e = await Assert.ThrowsAsync<DbalException>(() => connection.QuerySingle<User>("SELECT id, email FROM users"));
			Assert.Equal("Query must return exactly one row but returned zero (SELECT id, email FROM users)", e.Message);
		}

		[Fact]
		public async Task QuerySingle()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

			var user = await connection.QuerySingle<User>("SELECT id, email FROM users");

			Assert.IsType<User>(user);
			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}

		[Fact]
		public async Task QuerySingle_Anonymous_Throws_ReturnsNull()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var e = await Assert.ThrowsAsync<DbalException>(() => connection.QuerySingle("SELECT id, email FROM users"));
			Assert.Equal("Query must return exactly one row but returned zero (SELECT id, email FROM users)", e.Message);
		}

		[Fact]
		public async Task QuerySingle_Anonymous()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (id, email) VALUES (42, 'john@doe.com')");

			var user = await connection.QuerySingle("SELECT id, email FROM users");

			Assert.Equal(new Dictionary<string, object?>
			{
				["id"] = 42L,
				["email"] = "john@doe.com",
			}, user);
		}

		[Fact]
		public async Task QueryChunks()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('user01@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user02@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user03@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user04@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user05@example.com')");

			var users = await connection
				.QueryChunks<User>("SELECT id, email FROM users ORDER BY id", 1)
				.ToArrayAsync();
			
			Assert.Equal(new []
			{
				"user01@example.com",
				"user02@example.com",
				"user03@example.com",
				"user04@example.com",
				"user05@example.com",
			}, users.Select(user => user.Email));
		}

		[Fact]
		public async Task QueryChunks_Anonymous()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (email) VALUES ('user01@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user02@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user03@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user04@example.com')");
			await connection.Execute("INSERT INTO users (email) VALUES ('user05@example.com')");

			var users = await connection
				.QueryChunks("SELECT id, email FROM users ORDER BY id", 1)
				.ToArrayAsync();

			Assert.Equal(new IImmutableDictionary<string, object?>[]
			{
				new Dictionary<string, object?>
				{
					["id"] = 1L,
					["email"] = "user01@example.com",
				}.ToImmutableDictionary(),
				new Dictionary<string, object?>
				{
					["id"] = 2L,
					["email"] = "user02@example.com",
				}.ToImmutableDictionary(),
				new Dictionary<string, object?>
				{
					["id"] = 3L,
					["email"] = "user03@example.com",
				}.ToImmutableDictionary(),
				new Dictionary<string, object?>
				{
					["id"] = 4L,
					["email"] = "user04@example.com",
				}.ToImmutableDictionary(),
				new Dictionary<string, object?>
				{
					["id"] = 5L,
					["email"] = "user05@example.com",
				}.ToImmutableDictionary(),
			}, users);
		}

		[Fact]
		public async Task QuerySingle_WithNullableNotNull()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE books (id INTEGER PRIMARY KEY, editor_id INTEGER DEFAULT NULL)");
			await connection.Execute("INSERT INTO books (editor_id) VALUES (42)");

			var book = await connection.QuerySingle<Book>("SELECT id, editor_id FROM books");

			Assert.IsType<Book>(book);
			Assert.Equal(1, book.Id);
			Assert.Equal(42, book.EditorId);
		}

		[Fact]
		public async Task QuerySingle_WithNullableNull()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE books (id INTEGER PRIMARY KEY, editor_id INTEGER DEFAULT NULL)");
			await connection.Execute("INSERT INTO books (editor_id) VALUES (NULL)");

			var book = await connection.QuerySingle<Book>("SELECT id, editor_id FROM books");

			Assert.IsType<Book>(book);
			Assert.Equal(1, book.Id);
			Assert.Null(book.EditorId);
		}

		private static Connection CreateConnection()
		{
			return new Connection(new AdoDriver(new SqliteConnection("Data Source=:memory:")));
		}
	}
}
