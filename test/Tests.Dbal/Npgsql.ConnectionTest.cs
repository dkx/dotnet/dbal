using System;
using System.Linq;
using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Npgsql;
using DKX.DbalTests.Data;
using Npgsql;
using Xunit;

namespace DKX.DbalTests
{
	public class NpgsqlConnectionTest
	{
		[Fact]
		public async Task Execute_WithNestedTransaction()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, email TEXT NOT NULL)");

			await using (var transaction01 = await connection.BeginTransaction())
			{
				await connection.Execute("INSERT INTO users (id, email) VALUES (1, 'user01@example.com')");

				await using (var transaction02 = await connection.BeginTransaction())
				{
					await connection.Execute("INSERT INTO users (id, email) VALUES (2, 'user02@example.com')");
					
					await using (var transaction03 = await connection.BeginTransaction())
					{
						await connection.Execute("INSERT INTO users (id, email) VALUES (3, 'user03@example.com')");
						
						await using (var transaction04 = await connection.BeginTransaction())
						{
							await connection.Execute("INSERT INTO users (id, email) VALUES (4, 'user04@example.com')");
							await transaction04.Commit();
						}
						
						await transaction03.Rollback();
					}

					await transaction02.Commit();
				}

				await transaction01.Commit();
			}

			await using (var transaction01 = await connection.BeginTransaction())
			{
				await connection.Execute("INSERT INTO users (id, email) VALUES (5, 'user05@example.com')");
				await transaction01.Rollback();
			}

			var users = (await connection.Query<User>("SELECT id, email FROM users").ToArrayAsync())
				.Select(user => user.Email)
				.ToArray();

			Assert.Equal(2, users.Length);
			Assert.Contains("user01@example.com", users);
			Assert.Contains("user02@example.com", users);
		}
		
		[Fact]
		public async Task Transactional()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, email TEXT NOT NULL)");

			await connection.Transactional(async () =>
			{
				await connection.Execute("INSERT INTO users (id, email) VALUES (1, 'user01@example.com')");
			});

			try
			{
				await connection.Transactional(async () =>
				{
					await connection.Execute("INSERT INTO users (id, email) VALUES (2, 'user02@example.com')");
					throw new Exception();
				});
			}
			catch
			{
				// ignored
			}

			var userId = await connection.Transactional(async () =>
			{
				await connection.Execute("INSERT INTO users (id, email) VALUES (3, 'user03@example.com')");
				return 3;
			});

			Assert.Equal(3, userId);

			try
			{
				await connection.Transactional<int>(async () =>
				{
					await connection.Execute("INSERT INTO users (id, email) VALUES (4, 'user04@example.com')");
					throw new Exception();
				});
			}
			catch
			{
				// ignored
			}

			var emails = await connection.QueryScalar<string>("SELECT email FROM users ORDER BY id").ToArrayAsync();
			
			Assert.Equal(new[]
			{
				"user01@example.com",
				"user03@example.com",
			}, emails);
		}
		
		[Fact]
		public async Task Transactional_Nested()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, email TEXT NOT NULL)");

			await connection.Transactional(async () =>
			{
				await connection.Transactional(async () =>
				{
					await connection.Transactional(async () =>
					{
						await connection.Execute("INSERT INTO users (id, email) VALUES (1, 'user01@example.com')");
					});
					
					await connection.Execute("INSERT INTO users (id, email) VALUES (2, 'user02@example.com')");
					
					await connection.Transactional(async () =>
					{
						await connection.Transactional(async () =>
						{
							await connection.Execute("INSERT INTO users (id, email) VALUES (3, 'user03@example.com')");
						});
					});
				});

				await connection.Transactional(async () =>
				{
					await connection.Transactional(async () =>
					{
						await connection.Execute("INSERT INTO users (id, email) VALUES (4, 'user04@example.com')");
					});
					
					await connection.Execute("INSERT INTO users (id, email) VALUES (5, 'user05@example.com')");
					
					await connection.Transactional(async () =>
					{
						await connection.Execute("INSERT INTO users (id, email) VALUES (6, 'user06@example.com')");
					});
				});
			});

			var emails = await connection.QueryScalar<string>("SELECT email FROM users ORDER BY id").ToArrayAsync();
			
			Assert.Equal(new[]
			{
				"user01@example.com",
				"user02@example.com",
				"user03@example.com",
				"user04@example.com",
				"user05@example.com",
				"user06@example.com",
			}, emails);
		}

		[Fact]
		public async Task Query_WithEnums()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TYPE color_t AS ENUM('red', 'green', 'blue')");
			await connection.Execute("CREATE TABLE cars (id INT PRIMARY KEY, color color_t)");

			((NpgsqlConnection) connection.ToDriver().ToDbConnection()).ReloadTypes();
			((NpgsqlConnection) connection.ToDriver().ToDbConnection()).TypeMapper.MapEnum<Color>("color_t");

			await connection.Execute("INSERT INTO cars (id, color) VALUES (@Id, @Color)", new
			{
				Id = 1,
				Color = Color.Blue,
			});

			var car = await connection.QuerySingle<Car>("SELECT id, color FROM cars");

			Assert.Equal(1, car.Id);
			Assert.Equal(Color.Blue, car.Color);
		}

		[Fact]
		public async Task QueryChunks()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, email TEXT NOT NULL)");
			await connection.Execute("INSERT INTO users (id, email) VALUES (1, 'user01@example.com')");
			await connection.Execute("INSERT INTO users (id, email) VALUES (2, 'user02@example.com')");
			await connection.Execute("INSERT INTO users (id, email) VALUES (3, 'user03@example.com')");
			await connection.Execute("INSERT INTO users (id, email) VALUES (4, 'user04@example.com')");
			await connection.Execute("INSERT INTO users (id, email) VALUES (5, 'user05@example.com')");

			var users = await connection
				.QueryChunks<User>("SELECT id, email FROM users ORDER BY id", 1)
				.ToArrayAsync();
			
			Assert.Equal(new []
			{
				"user01@example.com",
				"user02@example.com",
				"user03@example.com",
				"user04@example.com",
				"user05@example.com",
			}, users.Select(user => user.Email));
		}

		[Fact]
		public async Task ExecuteWithNullParameter()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, name VARCHAR)");
			await connection.Execute("INSERT INTO users (id, name) VALUES (1, @Name)", new
			{
				Name = (string?) null,
			});

			var name = await connection.ExecuteScalar<string?>("SELECT name FROM users");

			Assert.Null(name);
		}

		[Fact]
		public async Task InsertWithInit()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INT PRIMARY KEY, name VARCHAR)");

			var q = connection.CreateQueryBuilder()
				.Insert("users", new
				{
					id = 1,
					name = "John Doe",
				})
				.ToQuery();
				
			await q.Execute();

			var count = await connection.ExecuteScalar<long>("SELECT COUNT(*) FROM users");

			Assert.Equal(1, count);
		}

		private static Connection CreateConnection()
		{
			var host = Environment.GetEnvironmentVariable("POSTGRES_HOST") ?? "localhost";
			var id = "test_" + Guid.NewGuid().ToString().Replace("-", "");

			using (var factoryConnection = new NpgsqlConnection($"Host={host};Username=postgres;Password=12345;Database=postgres"))
			{
				factoryConnection.Open();

				using (var command = factoryConnection.CreateCommand())
				{
					command.CommandText = $"CREATE DATABASE {id}";
					command.ExecuteNonQuery();
				}
			}

			return new Connection(new NpgsqlDriver(new NpgsqlConnection($"Host={host};Username=postgres;Password=12345;Database={id}")));
		}
	}
}
