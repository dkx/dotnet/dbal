using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using DKX.Dbal;
using Moq;
using Xunit;

namespace DKX.DbalTests
{
	public class TransactionTest
	{
		[Fact]
		public async Task Disposing()
		{
			var closed = false;
			var dbTransaction = new Mock<DbTransaction>();
			var transaction = new Transaction(dbTransaction.Object, () => closed = true);

			await transaction.DisposeAsync();

			Assert.True(closed);
			dbTransaction.Verify(m => m.DisposeAsync(), Times.Once);
		}

		[Fact]
		public async Task Commit()
		{
			var closed = false;
			var dbTransaction = new Mock<DbTransaction>();
			var transaction = new Transaction(dbTransaction.Object, () => closed = true);

			await transaction.Commit();

			Assert.True(closed);
			dbTransaction.Verify(m => m.CommitAsync(It.IsAny<CancellationToken>()), Times.Once);
		}

		[Fact]
		public async Task Rollback()
		{
			var closed = false;
			var dbTransaction = new Mock<DbTransaction>();
			var transaction = new Transaction(dbTransaction.Object, () => closed = true);

			await transaction.Rollback();

			Assert.True(closed);
			dbTransaction.Verify(m => m.RollbackAsync(It.IsAny<CancellationToken>()), Times.Once);
		}
	}
}
