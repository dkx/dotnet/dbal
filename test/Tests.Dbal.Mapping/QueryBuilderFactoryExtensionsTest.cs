using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Mapping;
using DKX.Dbal.Mapping.Attributes;
using Microsoft.Data.Sqlite;
using Xunit;

namespace DKX.DbalMappingTests
{
	public sealed class QueryBuilderFactoryExtensionsTest
	{
		[Fact]
		public async Task Select()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Select<User>();

			Assert.Equal("SELECT id c0, email c1, address_town c2, address_street c3 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}

		[Fact]
		public async Task Select_WithAlias()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Select<User>("u");

			Assert.Equal("SELECT u.id c0, u.email c1, u.address_town c2, u.address_street c3 FROM users u", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task Select_WithEmbedded_Null()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Select<User>();

			Assert.Equal("SELECT id c0, email c1, address_town c2, address_street c3 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
			Assert.Null(user.Address);
		}
		
		[Fact]
		public async Task Select_WithEmbedded()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email, address_town, address_street) VALUES ('john@doe.com', 'Prague', 'Rýmařovská 1')");
			var qb = connection.CreateQueryBuilder().Select<User>();

			Assert.Equal("SELECT id c0, email c1, address_town c2, address_street c3 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
			Assert.NotNull(user.Address);
			Assert.Equal("Prague", user.Address!.Town);
			Assert.Equal("Rýmařovská 1", user.Address!.Street);
		}
		
		[Fact]
		public async Task Select_WithEmbeddedAndCustomGroup()
		{
			await using var connection = await CreateConnection("addr");
			await connection.Execute("INSERT INTO users (email, addr_town, addr_street) VALUES ('john@doe.com', 'Prague', 'Rýmařovská 1')");
			var qb = connection.CreateQueryBuilder().Select<UserWithCustomAddressGroup>();

			Assert.Equal("SELECT id c0, addr_town c1, addr_street c2 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<UserWithCustomAddressGroup>();

			Assert.Equal(1, user.Id);
			Assert.NotNull(user.Address);
			Assert.Equal("Prague", user.Address!.Town);
			Assert.Equal("Rýmařovská 1", user.Address!.Street);
		}
		
		[Fact]
		public async Task Select_WithTransformer()
		{
			await using var connection = await CreateConnection("addr");
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Select<UserWithEmailTransformer>();

			Assert.Equal("SELECT id c0, email c1 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<UserWithEmailTransformer>();

			Assert.Equal(1, user.Id);
			Assert.Equal(new Email("john@doe.com"), user.Email);
		}

		[Fact]
		public async Task Insert()
		{
			await using var connection = await CreateConnection();
			var qb = connection.CreateQueryBuilder().Insert(new User {Email = "john@doe.com"});

			Assert.Equal("INSERT INTO users (address_street, address_town, email, id) VALUES (@p3, @p2, @p1, @p0)", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("john@doe.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
			Assert.Null(await connection.ExecuteScalar<string>("SELECT address_town FROM users"));
			Assert.Null(await connection.ExecuteScalar<string>("SELECT address_street FROM users"));
		}

		[Fact]
		public async Task Insert_WithEmbedded()
		{
			await using var connection = await CreateConnection();
			var qb = connection.CreateQueryBuilder().Insert(new User
			{
				Email = "john@doe.com",
				Address = new Address
				{
					Town = "NY",
					Street = "Unknown",
				},
			});

			Assert.Equal("INSERT INTO users (address_street, address_town, email, id) VALUES (@p3, @p2, @p1, @p0)", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("john@doe.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
			Assert.Equal("NY", await connection.ExecuteScalar<string>("SELECT address_town FROM users"));
			Assert.Equal("Unknown", await connection.ExecuteScalar<string>("SELECT address_street FROM users"));
		}

		[Fact]
		public async Task Insert_WithTransformer()
		{
			await using var connection = await CreateConnection();
			var qb = connection.CreateQueryBuilder().Insert(new UserWithEmailTransformer
			{
				Email = new Email("john@doe.com"),
			});

			Assert.Equal("INSERT INTO users (email, id) VALUES (@p1, @p0)", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("john@doe.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
		}

		[Fact]
		public async Task Update()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Update(new User {Email = "lord@voldemort.com"});
			
			Assert.Equal("UPDATE users SET address_street = @p2, address_town = @p1, email = @p0", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("lord@voldemort.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
		}

		[Fact]
		public async Task Update_WithEmbedded()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Update(new User
			{
				Email = "lord@voldemort.com",
				Address = new Address
				{
					Town = "NY",
					Street = "Unknown",
				},
			});
			
			Assert.Equal("UPDATE users SET address_street = @p2, address_town = @p1, email = @p0", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("lord@voldemort.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
			Assert.Equal("NY", await connection.ExecuteScalar<string>("SELECT address_town FROM users"));
			Assert.Equal("Unknown", await connection.ExecuteScalar<string>("SELECT address_street FROM users"));
		}

		[Fact]
		public async Task Update_WithTransformer()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Update(new UserWithEmailTransformer
			{
				Email = new Email("lord@voldemort.com"),
			});
			
			Assert.Equal("UPDATE users SET email = @p0", qb.ToSql());

			await qb.ToQuery().Execute();
			
			Assert.Equal("lord@voldemort.com", await connection.ExecuteScalar<string>("SELECT email FROM users"));
		}

		[Fact]
		public async Task Update_WithAlias()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder().Update(new User {Email = "lord@voldemort.com"}, "u");
			
			Assert.Equal("UPDATE users u SET address_street = @p2, address_town = @p1, email = @p0", qb.ToSql());
		}
		
		[Fact]
		public async Task Delete()
		{
			await using var connection = await CreateConnection();
			var qb = connection.CreateQueryBuilder().Delete<User>();

			Assert.Equal("DELETE FROM users", qb.ToSql());
		}

		private static async Task<IConnection> CreateConnection(string addressPrefix = "address")
		{
			var connection = new Connection(new AdoDriver(new SqliteConnection("Data Source=:memory:")));
			await connection.Execute($"CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL, {addressPrefix}_town TEXT DEFAULT NULL, {addressPrefix}_street TEXT DEFAULT NULL)");
			return connection;
		}

		[Table("users")]
		private record User
		{
			[Id("id")]
			public long Id { get; init; }
			
			[Column("email")]
			public string Email { get; init; }
			
			[Embedded(typeof(AddressMapping))]
			public Address? Address { get; init; }
		}

		[Table("users")]
		private record UserWithCustomAddressGroup
		{
			[Id("id")]
			public long Id { get; init; }
			
			[Embedded(typeof(AddressMapping), "Addr")]
			public Address? Address { get; init; }
		}

		[Table("users")]
		private record UserWithEmailTransformer
		{
			[Id("id")]
			public long Id { get; init; }
			
			[Column("email", typeof(EmailTransformer))]
			public Email Email { get; init; }
		}

		private record Address
		{
			public string Town { get; init; }

			public string Street { get; init; }
		}

		private class AddressMapping : IEmbeddable<Address>
		{
			public IEnumerable<string> Columns { get; } = new []
			{
				"town",
				"street",
			};

			public string CreateFullColumnName(string group, string column)
			{
				return $"{group.ToLower()}_{column}";
			}

			public Address? ToEmbedded(IDictionary<string, object?> values)
			{
				if (values["town"] == null || values["street"] == null)
				{
					return null;
				}
				
				return new()
				{
					Town = (string) values["town"]!,
					Street = (string) values["street"]!,
				};
			}

			public IDictionary<string, object?> ToDatabaseValue(Address data)
			{
				return new Dictionary<string, object?>
				{
					["town"] = data.Town,
					["street"] = data.Street,
				};
			}
		}
		
		private readonly struct Email
		{
			public Email(string value)
			{
				Value = value;
			}
			
			public string Value { get; }
		}

		private class EmailTransformer : ITransformer<Email>
		{
			public Email ToNetValue(object value)
			{
				if (value is string str)
				{
					return new Email(str);
				}

				throw new NotSupportedException();
			}

			public object ToDatabaseValue(Email value)
			{
				return value.Value;
			}
		}
	}
}
