using System;
using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Mapping;
using DKX.Dbal.Mapping.Attributes;
using Microsoft.Data.Sqlite;
using Xunit;

namespace DKX.DbalMappingTests
{
	public sealed class SelectQueryBuilderExtensionsTest
	{
		[Fact]
		public async Task From()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("id", "email")
				.From<User>();

			Assert.Equal("SELECT id, email FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task From_WithAlias()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("u.id", "u.email")
				.From<User>("u");

			Assert.Equal("SELECT u.id, u.email FROM users u", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task From_WithHiddenColumns_ThrowsNotSupported()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");

			var e = Assert.Throws<NotSupportedException>(() =>
			{
				connection.CreateQueryBuilder()
					.Select("id", "email", "DATE('now')")
					.From("users")
					.WithHiddenColumns("DATE('now')");
			});

			Assert.Equal("Hidden columns are supported only with property mapped results", e.Message);
		}
		
		[Fact]
		public async Task From_WithHiddenColumns()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("id", "email", "DATE('now')")
				.From<User>()
				.WithHiddenColumns("DATE('now')");

			Assert.Equal("SELECT id, email, DATE('now') FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task From_WithHiddenColumnsAsAliases()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("id", "email", "DATE('now') AS now")
				.From<User>()
				.WithHiddenColumns("now");

			Assert.Equal("SELECT id, email, DATE('now') AS now FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task AddColumns()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("DATE('now') AS now")
				.From<User>()
				.AddColumns<User>()
				.WithHiddenColumns("now");

			Assert.Equal("SELECT DATE('now') AS now, id c0, email c1 FROM users", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task AddColumns_WithAlias()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select("DATE('now') AS now")
				.From<User>("u")
				.AddColumns<User>("u")
				.WithHiddenColumns("now");

			Assert.Equal("SELECT DATE('now') AS now, u.id c0, u.email c1 FROM users u", qb.ToSql());

			var user = await qb.ToQuery().ToSingle<User>();

			Assert.Equal(1, user.Id);
			Assert.Equal("john@doe.com", user.Email);
		}
		
		[Fact]
		public async Task Join()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select<User>("u")
				.Join<Role>("u", "r", "u.role_id = r.id");

			Assert.Equal("SELECT u.id c0, u.email c1 FROM users u INNER JOIN roles r ON u.role_id = r.id", qb.ToSql());
		}
		
		[Fact]
		public async Task InnerJoin()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select<User>("u")
				.InnerJoin<Role>("u", "r", "u.role_id = r.id");

			Assert.Equal("SELECT u.id c0, u.email c1 FROM users u INNER JOIN roles r ON u.role_id = r.id", qb.ToSql());
		}
		
		[Fact]
		public async Task LeftJoin()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select<User>("u")
				.LeftJoin<Role>("u", "r", "u.role_id = r.id");

			Assert.Equal("SELECT u.id c0, u.email c1 FROM users u LEFT JOIN roles r ON u.role_id = r.id", qb.ToSql());
		}
		
		[Fact]
		public async Task RightJoin()
		{
			await using var connection = await CreateConnection();
			await connection.Execute("INSERT INTO users (email) VALUES ('john@doe.com')");
			var qb = connection.CreateQueryBuilder()
				.Select<User>("u")
				.RightJoin<Role>("u", "r", "u.role_id = r.id");

			Assert.Equal("SELECT u.id c0, u.email c1 FROM users u RIGHT JOIN roles r ON u.role_id = r.id", qb.ToSql());
		}

		private static async Task<IConnection> CreateConnection(string addressPrefix = "address")
		{
			var connection = new Connection(new AdoDriver(new SqliteConnection("Data Source=:memory:")));
			await connection.Execute($"CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL, {addressPrefix}_town TEXT DEFAULT NULL, {addressPrefix}_street TEXT DEFAULT NULL)");
			return connection;
		}

		[Table("users")]
		private record User
		{
			[Id("id")]
			public long Id { get; init; }
			
			[Column("email")]
			public string Email { get; init; }
		}

		[Table("roles")]
		private record Role
		{
			[Id("id")]
			public long Id { get; init; }
		}
	}
}
