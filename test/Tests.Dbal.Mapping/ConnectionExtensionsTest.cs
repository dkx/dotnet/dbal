using System.Threading.Tasks;
using DKX.Dbal;
using DKX.Dbal.Drivers;
using DKX.Dbal.Mapping;
using DKX.Dbal.Mapping.Attributes;
using Microsoft.Data.Sqlite;
using Xunit;

namespace DKX.DbalMappingTests
{
	public sealed class ConnectionExtensionsTest
	{
		[Fact]
		public async Task Insert()
		{
			await using var connection = CreateConnection();
			await connection.Execute("CREATE TABLE users (id INTEGER PRIMARY KEY, email TEXT NOT NULL)");

			var rows = await connection.Insert(new User {Email = "john@doe.com"});

			Assert.Equal(1, rows);
			
			rows = await connection.Insert(new User {Email = "lord@voldemort.com"});

			Assert.Equal(1, rows);
		}

		private static IConnection CreateConnection()
		{
			return new Connection(new AdoDriver(new SqliteConnection("Data Source=:memory:")));
		}

		[Table("users")]
		private record User
		{
			[Column("email")]
			public string Email { get; init; }
		}
	}
}
