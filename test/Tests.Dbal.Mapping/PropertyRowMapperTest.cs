using System;
using System.Collections.Generic;
using DKX.Dbal.Mapping;
using DKX.Dbal.Mapping.Attributes;
using Xunit;

namespace DKX.DbalMappingTests
{
	public sealed class PropertyRowMapperTest
	{
		[Fact]
		public void Map()
		{
			var mapper = new PropertyRowMapper();
			var user = mapper.Map<User>(typeof(User), new DictionaryDataRecord(new Dictionary<string, object>
			{
				["id"] = 42,
				["email"] = "john@doe.com",
			}));

			Assert.Equal(42, user.Id);
			Assert.Equal("john@doe.com", user.Email);
			Assert.Null(user.FavoriteFood);
		}
		
		[Fact]
		public void Map_Throws_MissingProperty()
		{
			var mapper = new PropertyRowMapper();
			var e = Assert.Throws<Exception>(() =>
			{
				mapper.Map<User>(typeof(User), new DictionaryDataRecord(new Dictionary<string, object>
				{
					["id"] = 42,
					["email"] = "john@doe.com",
					["moto"] = "lorem ipsum",
				}));
			});

			Assert.Equal($"Can not map record to '{typeof(User).FullName}', there are no writable properties for columns: moto", e.Message);
		}

		[Table("user")]
		private record User
		{
			[Id("id")]
			public int Id { get; init; }
			
			[Column("email")]
			public string Email { get; init; }
			
			public string? FavoriteFood { get; init; }
		}
	}
}
